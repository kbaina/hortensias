// @auteur Karim Baïna, ENSIAS, Since Decembre 2010, last update October 2019

1. Introduction

hortensias is a lightweight oss pedagocial language coming with two components hensiasc and hensiasi
hensiasc is a pedagocial compiler of hortensias language towards simple one-address code
hensiasi a pedagocial interpreter of the generated one-address code
hensiasc and hensiasi are all written in C language.

by pedagogical, I mean four things :

1. hortensias is a simple language for beginners in programming

hortensias is imperative language
hortensias takes inspiration from C, Ada, and other languages
hortensias is not 
case sentitive


2. hortensias comes with multilangual (for the moment : english, spanish, french, and german) error messages for non english speaking students


3. hortensias code is a complete pedagogical platform for learning how to build a LL(1) top down compiler, pseudo code generator, optimiser and pseudocode interpreter


4. hortensias code is a complete lab platform for teaching compiler engineering, and composing labs and exams

Hortensias compiler is oss language written in C. It comes with multilungual error management (spanish, french, german, and english but you implement your language if you want) his 1-address pseudo-code compiler/interpreter is java-like.
You can customise AST (rightassoc, leftassoc), activate code optimiser, and more to come Learn&Teach compiling semantics & pragmatics design and manual programming becomes easier. Enjoy using customising and improving it's GPL.

hortensias name is inspired from flowers name, but also contain ensias suffix which is Ecole Nationale Supérieure d'Informatique et d'Analyse des Systèmes, Mohammed V University in Rabat, Morocco where hortensias is built.
I invite you to first read LL1_hortensias_grammar.txt to learn about hortensias language LL(1) grammar.
I invite you to discover samples directory containing some hortensias program to begin with.
make all : compiles both the compiler & the interpreter
./hensiasc < ./sample/f.hos : launches the compiler on f.hos hostensias program
./hensiasi : launches the interpreter
./hensiasc < ./sample/f.hos | ./hensiasi
good discovery.

make all : compiles both the compiler & the interpreter
./hensiasc < ./samples/div_by_zero.hos : launches the compiler on f.hos hostensias program
./hensiasi : launches the interpreter
./hensiasc < ./samples/factorielle.hos | ./hensiasi
good discovery.


2. Hortensias syntax 

I invite you to first read LL1 hortensias grammar to learn about hortensias language.

PRE_PROG ::= PRAGMA_LIST PROG

PRAGMA_LIST ::= 'pragma' PRAGMA_LIST | 

PROG ::= DECL_LIST 'begin' INST_LIST 'end'

INST_LIST ::= INST INST_LIST_AUX

INST_LIST_AUX ::= INST_LIST  | 


DECL_LIST ::= DECL DECL_LIST_AUX 

DECL_LIST_AUX ::= DECL_LIST | 

DECL ::= 'idf' TYPE DECL_AUX


DECL_AUX ::= CONST ';' | ';'


TYPE ::= 'int' | 'bool' | 'double' | 'string'


CONST ::= 'inumber' | 'dnumber' | 'cstring' | 'true' | 'false'
 

INST ::= 'idf' = ASSIGN_AUX ';'
	| 'if' '(' 'idf' '==' ADDSUB ')' 'then' INST_LIST IF_INSTAUX 
	| 'print' 'idf' ';' | 'print' 'cstring' ';'
	| 'for' 'idf' '=' 'inumber' 'to' 'inumber' 'do' INST_LIST 'endfor'
	| 'switch' '(' 'idf' ')' SWITCH_BODY 'default' ':' INST_LIST 'break' ';' 'endswitch' 


ASSIGN_AUX ::= ADDSUB 
	| 'true'
	| 'false'

SWITCH_BODY ::= 'case' 'inumber' ':' LIST_INST 'break' ';'  SWITCH_BODYAUX

SWITCH_BODYAUX ::= SWITCH_BODY | 

IF_INSTAUX ::=  'endif'  | 'else' INST_LIST 'endif'

ADDSUB ::= MULTDIV ADDSUBAUX

ADDSUBAUX ::= '–' MULTDIV ADDSUBAUX
	| '+' MULTDIV ADDSUBAUX
	| 

MULTDIV::= AUX MULTDIVAUX

MULTDIVAUX ::= '*' MULTDIV
	| '/' MULTDIV
	| 

AUX ::= 'idf'
	| 'inumber' | 'dnumber'
	| '(' ADDSUB ')'

3. Hortensias pragmas :

3.1. Pragmas for multigual error management
#English 	// switch to english error handler (default one)
#French	// switch to french error handler
#Spanish	// switch to spanish error handler
#German	// switch to german error handler

The lattest mentioned in the code is the one that the compiler will choose for error management.

3.2. Pragma for optimisation management
#staticoptimiser	
Activate optimiser (by default the optimiser is not active)
operates ignoring of non used variables, transforming non modified variables to constants, computing in the code, and Dead path elimination.
For arithmetic ASTs, left-right-root (postorder) is always adopted for #leftassoc, and right-left-root (reverse postorder) is always adopted for #rightassoc in order to optimise stack use.

#dynamicoptimiser
Does whatever staticoptimiser operates but when handling arithmetic AST, deeper child pseudocode is generated first for better optimising stack use.


3.3. Pragma for binary operators semantics
#rightassoc 	// select a semantic analyser with right AST
This mode assigns a right associativity to all binary operators : +, -, /, * and enables generation of AST, and pseudo code related to it.

#lefttassoc 	// select a semantic analyser with left AST

This mode assigns a left associativity to all binary operators : +, -, /, * and enables generation of AST, and pseudo code related to it.

The lattest mentioned in the code is the one that the compiler will choose for error management.

a pragma may cancel the effect a previous pragma

a pragma may occur multiple times without verification.

3.4. Pragma for visualising AST, and controlflow graph

#controlflowgraph


4. Hortensias sample

I invite you to discover samples directory containing some hortensias program to begin with.

#spanish
#rightassoc
#optimise
	REM CALCUL DE EXP(P) avec PREMIER(P)
	REM PREMIER(P) : TOUT I in {2..SQRT(P)} I ne divise pas P
s1 string;
s2 string "hello world";
I int 0;
REVERSE int;
RESTE_DIVISION int;
NBDIVISEURS int 0;
RES bool;
FACTO double 1.0;
EXPO_X double 1 ;
PUISSANCE double 1.0;
N int 5473;
VI int 0;
VD Double ;
DERNIERDIVISEUR int;
begin
		PRINT s1;

		PRINT "Algorithme d'euclide appliqué à" ; PRINT N;

		REM PREMIER( N ) : POUR TOUT I in {2..SQRT( N )} I ne divise pas P
		FOR I = 2 to 74 do REM do 
				
			REVERSE = ((74 - I) + 2);
			
			RESTE_DIVISION = N - ((N / REVERSE) * REVERSE) ;
			
			IF (RESTE_DIVISION == 0) THEN
			    NBDIVISEURS = NBDIVISEURS + 1 ;
			    DERNIERDIVISEUR = REVERSE ;
			ENDIF
		ENDFOR
	
		REM SI PREMIER( N ) CALCUL DE EXP( N )
		IF (NBDIVISEURS == 0) THEN
		
    		    PRINT "Le nombre est premier :-) " ;
		    REM EXPO(X) = 1 + (X / 1!) + (X**2 / 2!) + (X**3 / 3!) + .. + (X**n / n!)

		    FOR I = 1 to 180 do
			PUISSANCE = PUISSANCE * N;

			FACTO = 2 * FACTO * I * 0.5;

			EXPO_X = EXPO_X + (1 / (FACTO / PUISSANCE));

		    ENDFOR

		    PRINT "Exponentiel du nombre premier est = " ; print EXPO_X;

		ELSE
		    PRINT "Le nombre n'est pas premier :-(" ;
		    PRINT "Son premier diviseur est =" ; PRINT DERNIERDIVISEUR;			
		    PRINT "Son nombre de divisieurs est =" ; PRINT NBDIVISEURS;
		ENDIF
end

5. Hortensias compiled pseudocode :
Hortensias language is compiled by hortensias compiler (hensiasc) to a portable one address pseudocode that is interpreted by hortensias interpreter (hensiasi) through a simple abstract machine :

 PSEUDOCODE : DATA BEGIN LISTE_INST_PSEUDOCODE END
 LISTE_INST_PSEUDOCODE : INST_PSEUDOCODE LISTE_INST_AUX_PSEUDOCODE
 LISTE_INST_AUX_PSEUDOCODE : LISTE_INST_PSEUDOCODE | epsilon
 DATA : DATA_ITEM DATA_AUX 
 DATA_AUX : DATA | epsilon
 DATA_ITEM : idfop CONST
 CONST : inumberop | dnumberopc| cstringop
 INST_PSEUDOCODE :
	ADDOP 		|
	DIVOP		|
	DUPLOP		|
	JMPOP LABELOP	|
	JNEOP LABELOP	|
	JGOP LABELOP	|
	JEQOP LABELOP	|
	LABELOP ':'	|
	LOADOP IDFOP	|
	MULTOP		|
	PRINTIDFOP		|
	PRINTSTROP		|
	PUSHOP CONST 	|
	SUBOP		|
	STOREOP	IDFOP	|
	SWAPOP

6. Hortensias compiled sample :

s1 ""
i 0.000000
reverse 0.000000
reste_division 0.000000
nbdiviseurs 0.000000
facto 1.000000
expo_x 1.000000
puissance 1.000000
n 5473.000000
dernierdiviseur 0.000000
begin:
LOAD s1
PRINTI
PRINTS "Algorithme d'euclide appliqué à"
LOAD n
PRINTI
PUSH 2.000000
STORE i
for0:
PUSH 74.000000
LOAD i
JG endfor0
PUSH 74.000000
LOAD i
SWAP
SUB
PUSH 2.000000
ADD
STORE reverse
LOAD n
LOAD n
LOAD reverse
SWAP
IDIV
LOAD reverse
MULT
SWAP
SUB
STORE reste_division
PUSH 0.000000
LOAD reste_division
JNE endif1
LOAD nbdiviseurs
PUSH 1.000000
ADD
STORE nbdiviseurs
LOAD reverse
STORE dernierdiviseur
endif1:
PUSH 1.000000
LOAD i
ADD
STORE i
JMP for0
endfor0:
PUSH 0.000000
LOAD nbdiviseurs
JNE else2
else2:
PRINTS "Le nombre n'est pas premier :-("
PRINTS "Son premier diviseur est ="
LOAD dernierdiviseur
PRINTI
PRINTS "Son nombre de divisieurs est ="
LOAD nbdiviseurs
PRINTI
endif2:
end:

7. Hortensias Initial contributor
Hortensias open source programming language, compiler and interpreter is a contribution of Karim Baïna.

Prof. Karim Baïna is full Professor, Dean of Software Engineering Department at ENSIAS (Ecole Nationale Supérieure d’Informatique et d’Analyse des Systèmes), Mohammed V University in Rabat, Morocco, since 2004. In 2015, he has obtained IBM "BigData Specialist Certificate", he is IBM MEA University Instructor (Trainer of Trainees) on IBM BigInsights Big Data Technologies, and has prepared to IBM BigInsights certification hundreds of faculty members, and students in Morocco, Tunisia, and South Africa between since 2015. He has been and Cooperation Service Responsible at ENSIAS leading international student exchanges with ENSIMAG/INPG (co-graduation) and ISIMA/UBP (co-graduation), ENSEIRB-Matematica/INPB,
ENSEEIHT/INPT, ENSI/Tunisia, DSV/Stockholm Uni Sweden, Aalto Finland, and Sherbrooke Canada). He has been Leader of Alqualsadi research team (Enterprise Architecture, Quality of their Development and Integration) in EA (Enterprise Architecture), SOA (Service Oriented Architecture), BPM (Business Process Management), and BPI (Business Process Intelligence). He has achieved multiple consulting and training projects in Morocco, Tunisia, France, Australia, Sweden, Italy, and Cameroon. He has participated, managed, and coached many important companies IT projects from pre-sales, design to production for telco industry. He has been active operational and management member in many EU, Tempus, and  DaaD, and SRC projects : MEDFORIST, MED-NETU, MED-IST, JOIN-MED, MENA, OPEN, PORFIRE. He has more than 90 international publications. He has defended his habilitation at EMI (Ecole Mohammadia d'Ingénieurs), Mohammed V University Rabat, Morocco, in 2007. He has obtained his PhD thesis in Computer Science from UHP (Université Henri Poincaré), Nancy 1, France, in 2003, after what he has achieved his potdoctoral project in UNSW (University of New South Wales), Sydney, Australia. He has obtained his engineering degree in Computer Science and Applied Mathematics from ENSIMAG (Ecole Nationale Supérieure d’Information et Mathématiques Appliquées), Grenoble, France in 1999. karim.baina@um5.ac.ma, twitter : @kbaina, slideshare : www.slideshare.net/kbaina.

8. Hortensias Gitlab repository
Licence : GPL
How to clone : git clone git@gitlab.com:kbaina/hortensias.git

