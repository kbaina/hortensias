#ifndef ANALYSEUR_H
#define ANALYSEUR_H

#include "ast.h"

// @auteur Karim Baïna, ENSIAS, Décembre 2018

typedef enum {
INUMBER = 1000,
IDF = 1001,
INT = 1002,
BOOL = 1003,
TRUE = 1004,
FALSE = 1005,
PVIRG = 1006,
BEG_IN = 1007,
END = 1008,
IF = 1009,
EQ = 1010,
THEN = 1011,
ELSE = 1012,
ENDIF = 1013,
POPEN = 1014,
PCLOSE = 1015,
EQEQ = 1016,
PLUS = 1017,
MINUS = 1018,
MULT = 1019,
DIV = 1020,
DNUMBER = 1021,
DOUBLE = 1022,
PRINT = 1023,
FOR = 1024,
TO = 1025,
DO = 1026,
ENDFOR = 1027,
SWITCH = 1028,
CASE = 1029,
BREAK = 1030,
DEFAULT =1031,
ENDSWITCH=1032,
DEUXPOINT=1033,
PRAGMA = 1034,
STRING = 1035, // string type
CSTRING = 1036, // const string
ERRORLEX = 1037
} typetoken;

void set_idf_attributes(char *name, int line);
void set_number_attributes(double value);
void set_string_attributes(char * s);
void set_token_attributes(int line);


typedef enum {noOptimisation, staticOptimisation, dynamicOptimisation} OptimisationMode;
typedef enum {leftAssociativity, rightAssociativity} AssociativityType;

void set_Optimisation_Mode( OptimisationMode optimisationMode );
OptimisationMode get_Optimisation_Mode( );

void set_Associativity_Type( AssociativityType associativityType );
AssociativityType get_Associativity_Type( );

void set_Verbose_Mode( boolean verboseMode );
boolean get_Verbose_Mode( );



#define VERSION "3.0"

#endif
