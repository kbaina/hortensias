#include "tablesymb.h"
#include "pseudocode.h"
#include "analyseur_hortensias.h"
#include "error.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// @ auteur Karim Baïna, ENSIAS, Décembre 2010 update Décembre 2018

#define debug false
#ifndef NULL
#define NULL ((void *) 0)
#endif

void print_Optimisation_Mode( OptimisationMode optimisationMode  );
void print_Associativity( AssociativityType associativityType  );


// affiche une pseudo insctruction selon le mode de visite (pseudocodeVisitMode = false (interpretation), pseudocodeVisitMode = false (code optimisation)
void afficher_pseudo_instruction(struct pseudoinstruction pi, boolean OptimisationVisitMode){

  // l'instruction est affichee (normalement) si mode normal
  // par contre, en mode optimisation, l'instruction ne sera affichée que si ell a été visitée par le simulateur

  if ( (OptimisationVisitMode == false) || ((OptimisationVisitMode == true) && (pi.visitee == true)) )  {

	  switch(pi.codop){
		  case ADD:    printf("ADD\n");  break;
		  case IDIV:   printf("IDIV\n"); break;
		  case DDIV:   printf("DDIV\n"); break;
		  case DUPL:   printf("DUPL\n"); break;
		  case LABEL:  printf("%s:\n",pi.param.label_name); break;
		  case LOAD:   printf("LOAD "); printf("%s\n",pi.param.var); break;
		  case _MULT:  printf("MULT\n"); break;
		  case POP:    printf("POP\n");  break;
		  case PUSH:   printf("PUSH "); 
			switch(pi.param._const.etype){
					case DBL:printf("%lf\n",dvalue(pi.param._const)); break; 
					case STR:printf("%s\n",svalue(pi.param._const)); break;
			}
				break;
		  case SUB:    printf("SUB\n");  break;
		  case STORE:  printf("STORE "); printf("%s\n",pi.param.var); break;
		  case SWAP:   printf("SWAP\n"); break;
		  case PRNTI:   printf("PRINTI\n");break;
		  case PRNTS:   printf("PRINTS ");printf("%s\n",svalue(pi.param._const));break; 
		  case JNE:    printf("JNE "); printf("%s\n",pi.param.label_name);break; 
		  case JG:    printf("JG "); printf("%s\n",pi.param.label_name);break; 
		  case JMP:    printf("JMP "); printf("%s\n",pi.param.label_name);break; 
		  case JEQ:    printf("JEQ "); printf("%s\n",pi.param.label_name);break; 
		  case DATA: if ((OptimisationVisitMode == false) || ((OptimisationVisitMode == true) && (pi.param.nv.utilisee == true))) { 
				// la date est affichee normalement en mode normal
				// par contre en mode optimisation, la data ne sera affichee que si elle est utilisée par les instructions de programme
				printf("%s ",pi.param.nv.name);
				switch(pi.param.nv.value.etype){
					case DBL:	printf("%lf\n",dvalue(pi.param.nv.value)); break;
					case STR:	if (strcmp(svalue(pi.param.nv.value),"")==0) {printf("\"");printf("\"\n");}
							else printf("%s\n",svalue(pi.param.nv.value));
							break;
				}

			      } break; 
  	}
   }
}

// precondition pc <> NULL
void  afficher_pseudo_code(pseudocode  pc){
   boolean OptimisationVisitMode = false;
   if (pc != NULL){
    afficher_pseudo_instruction(pc->first, OptimisationVisitMode);    
    afficher_pseudo_code(pc->next);
  }
}

// precondition pc <> NULL
// afiche un pseudocode optimise (que les pseudoinstructions visitees)
void afficher_pseudo_code_optimise(pseudocode pc){
   boolean OptimisationVisitMode = true;
   if (pc != NULL){
    afficher_pseudo_instruction(pc->first, OptimisationVisitMode);    
    afficher_pseudo_code_optimise(pc->next);
  }
}

// precondition pc1 <> NULL et pc2 <> NULL
void inserer_code_en_queue(pseudocode  pc1, pseudocode pc2){
  
  if (debug) { 
    printf("inserer_code_en_queue(X, Y)\n");
    printf("X= ");afficher_pseudo_code(pc1);
    printf("Y= ");afficher_pseudo_code(pc2);
  }

  if (pc1->next == NULL) {
    pc1->next = pc2;
  }else{
    pseudocode pc = pc1;
    while(pc->next != NULL) {
      pc = pc->next;
    }
    
    pc->next = pc2;
  }
  if (debug) { 
    printf("X.Y=");afficher_pseudo_code(pc1);
    printf("\n");
    printf("out inserer_code_en_queue()\n");
  }
}

// declare que l'instruction est visitee
// l'optimisateur ne générera que les instructions visitees
void marquer_instruction_visitee(struct pseudoinstruction * ppi){
	ppi->visitee = true;
}



// génère le pseudo-code relatif à l'AST
// précondition ast <> NULL
/*
static OptimisationMode optimisationMode = noOptimisation; // by default the optimizer is not ON [generate left AST child first when leftAssociativity. generate left AST child first when rightAssociativity. other basic optimisations : transform not modified variable to constant, etc.]
static AssociativityType associativityType = leftAssociativity ; // by default the semantic analyser takes leftAssociativity for +, -, *, /

*/

pseudocode generer_pseudo_code_ast(AST ast, OptimisationMode optimisationMode, AssociativityType associativityType){
  pseudocode  pc = (pseudocode)malloc(sizeof (struct pseudocodenode));
  pseudocode valg, vald,pcswap;
  int profondeur_droite, profondeur_gauche;
  boolean left_ast_is_deeper;
  boolean ComputingInTheCode = false;
  pseudocode pdroit, pgauch;
  boolean ObviousComputing = false;

  int localrangvar;
  
  switch(ast->typeexp) {
  case NB :
    pc->first.codop = PUSH;
    //setdvalue(&(pc->first.param._const), ast->noeud.nombre);
    setdvalue(&(pc->first.param._const), ast->noeud.nombre);
    break;

  case _IDF :
    /* Verification que l'identificateur est a été modifier avant */

    if (optimisationMode != noOptimisation){ // la transforamtion d'une variabale en constante si on opère en mode optimisation.
	if (inTS(ast->noeud.idf, &localrangvar) == true && est_modifiee(localrangvar) == false) { 
            pc->first.codop = PUSH;
            Element e = valinit(localrangvar);
            setdvalue(&(pc->first.param._const), e.evalue.dvalue);
	}else{ 
	// variable is modified
		pc->first.codop = LOAD;
        	pc->first.param.var = ast->noeud.idf;
	}
    } else { // no optimisation RESERVE KB */
        pc->first.codop = LOAD;
        pc->first.param.var = ast->noeud.idf;
    } 
    break;

  case OP :
        profondeur_droite = calculer_profondeur_ast(arbre_droit(ast));
        profondeur_gauche = calculer_profondeur_ast(arbre_gauche(ast));
       
    	valg = generer_pseudo_code_ast(arbre_gauche(ast), optimisationMode, associativityType);
	vald = generer_pseudo_code_ast(arbre_droit(ast), optimisationMode, associativityType);


	/* Trouver la fin du pseudocode */
	
	///*KB RESERVE 
        pdroit = vald;
        while (pdroit->next != NULL) pdroit = pdroit->next;

        pgauch = valg;
        while (pgauch->next != NULL) pgauch = pgauch->next;
	//KB RESERVE */

        /* Vérification qu'aucune variable n'existe */
	///*KB RESERVE 

	if (optimisationMode != noOptimisation){ // Dans le case d'une optimisation on vérifie les conditions sont réalisées pour les optimisations ComputingITheCode et ObviousComputing
	        if ((pdroit->first.codop == PUSH) && (pgauch->first.codop == PUSH)) {
			ComputingInTheCode = true;
			if (debug) { 
			printf("ComputingInTheCode [ "); afficher_pseudo_code(valg); printf(" --OP %s--",((top(ast)==plus)?"+":((top(ast)==moins)?"-":((top(ast)==mult)?"*":"/" ) ) ) ); afficher_pseudo_code(vald); printf(" ]");
			}

		}
		//KB RESERVE */
	
        	/* Verification qu'il n'y a pas d'éléments absorbant dans l'opération arithmétique */
        	/* Verification d'une multiplication par 0 */
		///* KB RESERVE 
	
        	else if (pdroit->first.codop == PUSH && pdroit->first.param._const.evalue.dvalue == 0.0 && ast->noeud.op.top == mult) {
        	    pc->first.codop = PUSH;
        	    pc->first.param._const.evalue.dvalue = 0.0;
        	
        	    ObviousComputing = true;
	
		    if (debug) { printf("ObviousComputing [ MULT by 0 à droite ]");  }
        	} else if (pgauch->first.codop ==PUSH && pgauch->first.param._const.evalue.dvalue == 0.0 && ast->noeud.op.top == mult) {
        	    pc->first.codop = PUSH;
        	    pc->first.param._const.evalue.dvalue = 0.0;
        	    
        	    ObviousComputing = true;
		    if (debug) { printf("ObviousComputing [ MULT by 0 à gauche ]");  }
        	}
		//KB RESERVE */
	
        	/* Verification d'une multiplication par 1 à gauche et à droite*/
		///* KB RESERVE 
        	else if (pdroit->first.codop == PUSH && pdroit->first.param._const.evalue.dvalue == 1 && ast->noeud.op.top == mult) {
        	    pc = pgauch;
        	
        	    ObviousComputing = true;
		  if (debug) { printf("ObviousComputing [ MULT by 1 à droite ]");  }
        	} else if (pgauch->first.codop ==PUSH && pgauch->first.param._const.evalue.dvalue == 1 && ast->noeud.op.top == mult) {
        	    pc = pdroit;
        	    
        	    ObviousComputing = true;
		  if (debug) { printf("ObviousComputing [ MULT by 1 à gauche ]");  }
        	}
       		//KB RESERVE */
	
	
        	/* Verification d'une addition de 0 à gauche et à droite */
		///* KB RESERVE 
        	else if (pdroit->first.codop == PUSH && pdroit->first.param._const.evalue.dvalue == 0 && ast->noeud.op.top == plus) {
        	    pc = pgauch;
        	
        	    ObviousComputing = true;
		  if (debug) { printf("ObviousComputing [ ADD of 0 à droite ]");  }
        	} else if (pgauch->first.codop ==PUSH && pgauch->first.param._const.evalue.dvalue == 0 && ast->noeud.op.top == plus) {
        	    pc = pdroit;
        	    
        	    ObviousComputing = true;
		  if (debug) { printf("ObviousComputing [ ADD of 0 à gauche ]");  }
        	}
    		//KB RESERVE */
        	/* Verification d'une soustraction de 0 */
    		///* KB RESERVE
        	else if (pdroit->first.codop == PUSH && pdroit->first.param._const.evalue.dvalue == 0 && ast->noeud.op.top == moins) {
        	    pc = pgauch;
        	
        	    ObviousComputing = true;
		  if (debug) { printf("ObviousComputing [ SUB of 0 ]");  }
        	} 
       		//KB RESERVE */
        	/* Verification d'une division par 1 : */
    		///* KB RESERVE
        	else if (pdroit->first.codop == PUSH && pdroit->first.param._const.evalue.dvalue == 1 && ast->noeud.op.top == _div) {
        	    pc = pgauch;
        	
        	    ObviousComputing = true;
		  if (debug) { printf("ObviousComputing [ DIV by 1 ]");  }
        	} 
       		//KB RESERVE */
        	/* Verification qu'on ne divise pas 0 / X ==> PUSH 0 */
    		///* KB RESERVE
        	else if (pgauch->first.codop == PUSH && pgauch->first.param._const.evalue.dvalue == 0 && ast->noeud.op.top == _div) {
        	    pc->first.codop = PUSH;
        	    pc->first.param._const.evalue.dvalue = 0.0;
        	    
        	    ObviousComputing = true;
		  if (debug) { printf("ObviousComputing [ DIV 0 over something ]");  }
        	}
    		//KB RESERVE */
        	/* Vérification que l'on ne soustraie par l'élément de lui même */
    		///* KB RESERVE
        	else if (pgauch->first.codop == LOAD && pdroit->first.codop == LOAD && strcmp(pgauch->first.param.var,pdroit->first.param.var) == 0 && ast->noeud.op.top == moins) {
        	    pc->first.codop = PUSH;
        	    pc->first.param._const.evalue.dvalue = 0.0;
	
        	    ObviousComputing = true;
		  if (debug) { printf("ObviousComputing [ SUB X by X ]");  }
        	}
    		//KB RESERVE */
        	    /* Vérification que l'on ne divise pas l'élément par lui même */
    		///* KB RESERVE
        	else if (pgauch->first.codop == LOAD && pdroit->first.codop == LOAD && strcmp(pgauch->first.param.var,pdroit->first.param.var) == 0 && ast->noeud.op.top == _div) {
        	    pc->first.codop = PUSH;
        	    pc->first.param._const.evalue.dvalue = 1;
	
        	    ObviousComputing = true;
		  if (debug) { printf("ObviousComputing [ DIV X by X ]");  }
        	}
    		//KB RESERVE */
	}
        
	left_ast_is_deeper = (profondeur_gauche > profondeur_droite) ;
	// on évite le >= pour éviter l'opération SWAP ;-)
	
    	pc->first.line = ast->noeud.op.line; // la ligne permet de tracer les problèmes d'opération (exemple division par zéro)

	///* KB RESERVE
        if ((ComputingInTheCode == false) && (ObviousComputing == false)) {
   	 // KB RESERVE */
	    switch(ast->noeud.op.top){
		    case plus :// if (debug) printf("%lf + %lf == %lf\n",valg, vald, valg + vald);
		      pc->first.codop = ADD; break;
		      pc->next = NULL;

		    case moins : // if (debug) printf("%lf - %lf == %lf\n",valg, vald, valg - vald);
		      pc->first.codop = SUB; // opération non commutative
		      pc->next = NULL;
			// the swap is necessary in three cases (G-D-R left-right-root generation) :
			// 1. no static optimisation (with all kinds of associativity)
			// 2. static optimisation and leftAssociativity
			// 3. dynamic optimisation and left ast is deeper than right ast
		      if (	(   optimisationMode == noOptimisation ) ||
			( ( optimisationMode == staticOptimisation ) && ( associativityType == leftAssociativity ) ) || 
	 		( ( optimisationMode == dynamicOptimisation ) && ( left_ast_is_deeper == true ) )) {  
			      pcswap = (pseudocode)malloc(sizeof (struct pseudocodenode));
			      pcswap->first.codop = SWAP;
			      pcswap->next = pc;
			      pc = pcswap;
      			}
		      break;

		    case mult : // if (debug) printf("%lf * %lf == %lf\n",valg, vald, valg * vald);
		      /* Verification que ne fait pas une mutliplication par 0 */
		      pc->first.codop = _MULT;
		      pc->next = NULL;
		      break;

		    case _div : // if (debug) printf("%lf - %lf == %lf\n",valg, vald, valg - vald);
		      if (ast->typename == Double){
			      // division réelle
			      pc->first.codop = DDIV; // opération non commutative
		      }else{
			      // division entière
			      pc->first.codop = IDIV; // opération non commutative
		      }
		      pc->next = NULL;
			// the swap is necessary in three cases (G-D-R left-right-root generation) :
			// 1. no static optimisation (with all kinds of associativity)
			// 2. static optimisation and leftAssociativity
			// 3. dynamic optimisation and left ast is deeper than right ast
		      if (	(   optimisationMode == noOptimisation ) ||
			( ( optimisationMode == staticOptimisation ) && ( associativityType == leftAssociativity ) ) || 
		 	( ( optimisationMode == dynamicOptimisation ) && ( left_ast_is_deeper == true ) )) {  
			      pcswap = (pseudocode)malloc(sizeof (struct pseudocodenode));
			      pcswap->first.codop = SWAP;
			      pcswap->next = pc;
			      pc = pcswap;
      			}
		      break;
	    } //end switch
	    // G-D-R left-right-root code generation is operated in three cases :
		// 1. no static optimisation (with all kinds of associativity)
		// 2. static optimisation and leftAssociativity
		// 3. dynamic optimisation and left ast is deeper than right ast
	    if (	(   optimisationMode == noOptimisation ) ||
		( ( optimisationMode == staticOptimisation ) && ( associativityType == leftAssociativity ) ) || 
	 	( ( optimisationMode == dynamicOptimisation ) && ( left_ast_is_deeper == true ) )) {  

			if (debug) {
				printf("--->G-D-R");
				print_Optimisation_Mode( optimisationMode ); printf("\n");
				print_Associativity( associativityType ); printf("\n");
			}
		    inserer_code_en_queue(valg, vald);
		    inserer_code_en_queue(vald, pc);        
		    pc = valg;
	    } else  {      // D-G-R right-left-root when right associativity with dynamic optimising stack
			if (debug){ 
				printf("--->D-G-R\n"); 
				print_Optimisation_Mode( optimisationMode ); printf("\n");
				print_Associativity( associativityType ); printf("\n");
			}
		    inserer_code_en_queue(vald, valg);
		    inserer_code_en_queue(valg, pc);            
		    pc = vald;
    	    }
	   ///*KB RESERVE
        } else if ( (optimisationMode != noOptimisation) && (ComputingInTheCode == true) ){
		if (debug) { printf("ComputingInTheCode == true");}
        	pc->first.codop = PUSH;

        	switch (ast->noeud.op.top) {
        	    case plus:
        	        setdvalue(&(pc->first.param._const), pdroit->first.param._const.evalue.dvalue + pgauch->first.param._const.evalue.dvalue);
			if (debug) { printf("setdvalue(&(pc->first.param._const), %s)", myitoa(pdroit->first.param._const.evalue.dvalue + pgauch->first.param._const.evalue.dvalue));}
        	        break;
        	    case moins:
        	        setdvalue(&(pc->first.param._const), pgauch->first.param._const.evalue.dvalue - pdroit->first.param._const.evalue.dvalue);
			if (debug) { printf("setdvalue(&(pc->first.param._const), %s)", myitoa(pdroit->first.param._const.evalue.dvalue - pgauch->first.param._const.evalue.dvalue));}
        	        break;
        	    case mult:
        	        setdvalue(&(pc->first.param._const), pdroit->first.param._const.evalue.dvalue * pgauch->first.param._const.evalue.dvalue);
			if (debug) { printf("setdvalue(&(pc->first.param._const), %s)", myitoa(pdroit->first.param._const.evalue.dvalue * pgauch->first.param._const.evalue.dvalue));}
        	        break;
        	    case _div:
			if (pdroit->first.param._const.evalue.dvalue == 0) { // toujours gérer la division par zéro (dans tous les modes de visite du pseudocode)
				creer_sm_erreur(DivisionbyZero, pc->first.line, NULL); // line may be either IDIV pseudocode instruction line used by interpreter or initial / code operateur assigned by generator/oprimiser
				setdvalue(&(pc->first.param._const), 0); // code qui ne sert qu'à ne pas causer de problème de structure du code erroné et retourné un pseudocode erroné mais structurellement bien formé
			}else{
	        	        setdvalue(&(pc->first.param._const), pgauch->first.param._const.evalue.dvalue / pdroit->first.param._const.evalue.dvalue);
				if (debug) { printf("setdvalue(&(pc->first.param._const), %s)", myitoa(pdroit->first.param._const.evalue.dvalue / pgauch->first.param._const.evalue.dvalue));}
			}
        	        break;
        	}
    	} // KB RESERVE */

    /* 
    if ( (leftAssociativity == true) || (optimisationMode == false)) { // G-D-R left-right-root when left associativity or (non optimising stack mode with all kinds of associativities)
	    inserer_code_en_queue(valg, vald);
	    inserer_code_en_queue(vald, pc);        
	    pc = valg;

    } else  {      // D-G-R right-left-root when right associativity with optimising stack :  (rightAssociativity == true) && (optimisationMode == true))
	    inserer_code_en_queue(vald, valg);
	    inserer_code_en_queue(valg, pc);            
	    pc = vald;
    }
    */

    // (rightAssociativity == true) && (optimisationMode == false)) will yield right AST but non optimised stack with G-D-R left-right-root

    break;
  }
 
  if (debug) { printf("RETURN [");afficher_pseudo_code ( pc ); printf(" ]\n");}
  return pc;
}

pseudocode generer_pseudo_code_inst(instvalueType instattribute, OptimisationMode optimisationMode, AssociativityType associativityType){
  static int label_index = 0;
  pseudocode  pc, pc1,pc2,pc3,pc31,pc4,pc5,pc6,pc7, pc8, pc9, pc10, pc11, pc12;
  pseudocode  rexpcode;
  char * label_name;
  char *label_num;

  switch(instattribute.typeinst){
  case Switch :

    if (debug) printf("Switch");
    pc = NULL;

    label_num = myitoa ( label_index++ );

    // if value of X, exist among switch value then jump to lvi and exit else jump to default
    int i = 0;
    int value;
    pseudocode  valuelabelcode, previousvaluelabelcode= NULL, firstvaluelabelcode=NULL;
    char ** labelname = (char**) malloc ((sizeof (char *)) * instattribute.node.switchnode.nbcases);
    for (i=0; i < instattribute.node.switchnode.nbcases; i++){
	// chargement de la valeur de l'index du switch
	value = instattribute.node.switchnode.cases[ i ].value;
	pc1 = (pseudocode)malloc(sizeof (struct pseudocodenode)); 
	if (pc == NULL) pc = pc1; // on se souvient de la tête de la liste à retourner à la fin
	pc1->first.codop = LOAD;
	pc1->first.param.var = name(instattribute.node.switchnode.rangvar);
	pc1->next = NULL;

	pc2 = (pseudocode)malloc(sizeof (struct pseudocodenode));
	pc2->first.codop = PUSH;
	setdvalue(&(pc2->first.param._const) , value);

	pc2->next = NULL;
	pc1->next = pc2 ;

	pc3 = (pseudocode)malloc(sizeof (struct pseudocodenode));    
    	pc3->first.codop = JEQ;
        labelname[ i ] = (char*) malloc(9+strlen(label_num)+strlen(myitoa(value)));
    	strcpy( labelname[ i ], "switch");
    	strcat( labelname[ i ], "_");
    	strcat( labelname[ i ], label_num);
    	strcat( labelname[ i ], "_");
    	strcat( labelname[ i ], myitoa(value));
	pc3->first.param.label_name = labelname[ i ];

	pc3->next = NULL;
	pc2->next = pc3;
	if (previousvaluelabelcode != NULL) inserer_code_en_queue(previousvaluelabelcode,pc1);
	previousvaluelabelcode = pc1;
    }

	pc4 = (pseudocode)malloc(sizeof (struct pseudocodenode));    
    	pc4->first.codop = JMP; // 
        pc4->first.param.label_name = (char*) malloc(8+strlen(label_num));
    	strcpy( pc4->first.param.label_name, "default");
    	strcat( pc4->first.param.label_name, label_num);
	
    	pc4->next = NULL;

	inserer_code_en_queue(pc,pc4);

    // generate liste des label lvi (foreach value vi)
    valuelabelcode, previousvaluelabelcode= NULL, firstvaluelabelcode=NULL;
    for (i=0; i < instattribute.node.switchnode.nbcases; i++){
	value = instattribute.node.switchnode.cases[ i ].value;
        valuelabelcode = (pseudocode)malloc(sizeof (struct pseudocodenode));    
	if (firstvaluelabelcode == NULL) firstvaluelabelcode = valuelabelcode; //se rappeler de la tête de la liste
    	valuelabelcode->first.codop = LABEL;
    	valuelabelcode->first.param.label_name = labelname[ i ];
	valuelabelcode->next = generer_pseudo_code_list_inst(instattribute.node.switchnode.cases[ i ].casebodylinst, optimisationMode, associativityType);
	
	pc5 = (pseudocode)malloc(sizeof (struct pseudocodenode));    
    	pc5->first.codop = JMP; // 
        pc5->first.param.label_name = (char*) malloc(10+strlen(label_num));
        strcpy( pc5->first.param.label_name, "endswitch");
        strcat( pc5->first.param.label_name, label_num);

	inserer_code_en_queue(valuelabelcode, pc5);

	if (previousvaluelabelcode != NULL) inserer_code_en_queue(previousvaluelabelcode,valuelabelcode);
	previousvaluelabelcode = valuelabelcode;
    }

	inserer_code_en_queue(pc4,firstvaluelabelcode);

    // label default
    pc6 = (pseudocode)malloc(sizeof (struct pseudocodenode));
    pc6->first.codop = LABEL;
    pc6->first.param.label_name = pc4->first.param.label_name;
    pc6->next = generer_pseudo_code_list_inst(instattribute.node.switchnode.defaultbodylinst, optimisationMode, associativityType);

		inserer_code_en_queue(firstvaluelabelcode, pc6);

    // endswitch
    pc7 = (pseudocode)malloc(sizeof (struct pseudocodenode));
    pc7->first.codop = LABEL;
    pc7->first.param.label_name = pc5->first.param.label_name;

    pc7->next = NULL;

		inserer_code_en_queue(pc6, pc7);

  break;

  case PrintIdf :
    if (debug) printf("PrintIdf");
    // NOP
    pc = (pseudocode)malloc(sizeof (struct pseudocodenode));
    pc->first.codop = LOAD;
    pc->first.param.var = name(instattribute.node.printnode.rangvar);
    rexpcode = (pseudocode)malloc(sizeof (struct pseudocodenode));
    rexpcode->first.codop = PRNTI;
    rexpcode->next = NULL;
    pc->next = rexpcode;
    break;

  case PrintString :
    if (debug) printf("PrintStr");
    // NOP
    pc = (pseudocode)malloc(sizeof (struct pseudocodenode));
    pc->first.codop = PRNTS;
    setsvalue(&(pc->first.param._const), instattribute.node.printnode.str);
    pc->next = NULL;
    break;
    
  case AssignArith :
    if (debug) printf("AssignArith");
    rexpcode = generer_pseudo_code_ast(instattribute.node.assignnode.right, optimisationMode, associativityType);
    if (debug) afficher_pseudo_code(rexpcode);
    pc = (pseudocode)malloc(sizeof (struct pseudocodenode));
    pc->first.codop = STORE;
    pc->first.param.var = name(instattribute.node.assignnode.rangvar);
    pc->next = NULL;
    inserer_code_en_queue(rexpcode, pc);
    pc = rexpcode;
    if (debug) afficher_pseudo_code(pc);
    break;

  case AssignBool :
    if (debug) printf("AssignBool");
    pc = (pseudocode)malloc(sizeof (struct pseudocodenode));
    pc->first.codop = PUSH;
    setdvalue(&(pc->first.param._const) , (double) (((instattribute.node.assignnode.right)->noeud).bool));

    pc1 = (pseudocode)malloc(sizeof (struct pseudocodenode));  
    pc1->first.codop = STORE;
    pc1->first.param.var = name(instattribute.node.assignnode.rangvar);
    pc1->next = NULL;

    pc->next = pc1;
   
    if (debug) afficher_pseudo_code(pc);
    break;
    
  case IfThenArith :
    if (debug) printf("If");
    
    pc = generer_pseudo_code_ast(instattribute.node.ifnode.right, optimisationMode, associativityType);

    pc1 = (pseudocode)malloc(sizeof (struct pseudocodenode));
    pc1->first.codop = LOAD;
    pc1->first.param.var = name(instattribute.node.ifnode.rangvar);
    pc1->next = NULL;

    pc2 = (pseudocode)malloc(sizeof (struct pseudocodenode));    
    pc2->first.codop = JNE;
    label_num=myitoa(label_index++);
    pc2->first.param.label_name = (char*) malloc(6+strlen(label_num));
    strcpy( pc2->first.param.label_name, "endif");
    strcat( pc2->first.param.label_name, label_num);

    if (debug)    printf("label == %s", pc2->first.param.label_name);
    pc2->next = NULL;

    pc3 = generer_pseudo_code_list_inst(instattribute.node.ifnode.thenlinst , optimisationMode, associativityType);

    // label endif
    pc4 = (pseudocode)malloc(sizeof (struct pseudocodenode));
    pc4->first.codop = LABEL;
    pc4->first.param.label_name = pc2->first.param.label_name;
    pc4->next = NULL;

    inserer_code_en_queue(pc3, pc4);
    pc2->next = pc3;
    pc1->next = pc2;
    inserer_code_en_queue(pc, pc1);

    if (debug)   {printf("debut-- ");afficher_pseudo_code(pc);printf(" --fin");}
    break;

  case IfThenElseArith :
    if (debug) printf("If");
    
    pc = generer_pseudo_code_ast(instattribute.node.ifnode.right, optimisationMode, associativityType);

    pc1 = (pseudocode)malloc(sizeof (struct pseudocodenode));
    pc1->first.codop = LOAD;
    pc1->first.param.var = name(instattribute.node.ifnode.rangvar);
    pc1->next = NULL;

    pc2 = (pseudocode)malloc(sizeof (struct pseudocodenode));    
    pc2->first.codop = JNE;
    label_num=myitoa(label_index++);
    pc2->first.param.label_name = (char*) malloc(6+strlen(label_num));
    strcpy( pc2->first.param.label_name, "else");
    strcat( pc2->first.param.label_name, label_num);
    if (debug)    printf("label == %s", pc2->first.param.label_name);
    pc2->next = NULL;

    pc3 = generer_pseudo_code_list_inst(instattribute.node.ifnode.thenlinst , optimisationMode, associativityType);

    pc31 = (pseudocode)malloc(sizeof (struct pseudocodenode));    
    pc31->first.codop = JMP;
    pc31->first.param.label_name = (char*) malloc(6+strlen(label_num));
    strcpy( pc31->first.param.label_name, "endif");
    strcat( pc31->first.param.label_name, label_num);

    pc4 = (pseudocode)malloc(sizeof (struct pseudocodenode));
    pc4->first.codop = LABEL;
    pc4->first.param.label_name = pc2->first.param.label_name;
    pc4->next = NULL;

    pc31->next = pc4;

    pc5 = generer_pseudo_code_list_inst(instattribute.node.ifnode.elselinst , optimisationMode, associativityType);   
    pc4->next = pc5;
   
    pc6 = (pseudocode)malloc(sizeof (struct pseudocodenode));
    pc6->first.codop = LABEL;
    pc6->first.param.label_name = (char*) malloc(strlen( pc31->first.param.label_name)+1);
    strcpy( pc6->first.param.label_name,  pc31->first.param.label_name);
    pc6->next = NULL;

    inserer_code_en_queue(pc5, pc6);
    inserer_code_en_queue(pc3, pc31);
    pc2->next = pc3;
    pc1->next = pc2;
    inserer_code_en_queue(pc, pc1);

    if (debug) {printf("debut-- ");afficher_pseudo_code(pc);printf(" --fin");}

    break;
  
    case For:

    // affectation i = borneinf    
    pc = (pseudocode)malloc(sizeof (struct pseudocodenode));
    pc->first.codop = PUSH;
    setdvalue(&(pc->first.param._const) , (double) (instattribute.node.fornode.borneinf));

    pc1 = (pseudocode)malloc(sizeof (struct pseudocodenode));  
    pc1->first.codop = STORE;
    pc1->first.param.var = name(instattribute.node.fornode.rangvar);

    pc->next = pc1;

    // le début du for
    pc2 = (pseudocode)malloc(sizeof (struct pseudocodenode));    
    pc2->first.codop = LABEL;
    label_num=myitoa(label_index++);    
    pc2->first.param.label_name = (char*) malloc(4+strlen(label_num));
    strcpy( pc2->first.param.label_name, "for");
    strcat( pc2->first.param.label_name, label_num);

    pc1->next = pc2;

    // test si i > bornesup
    pc3 = (pseudocode)malloc(sizeof (struct pseudocodenode));  
    pc3->first.codop = PUSH;
    setdvalue( &(pc3->first.param._const), (double) (instattribute.node.fornode.bornesup));

    pc2->next = pc3;

    pc4 = (pseudocode)malloc(sizeof (struct pseudocodenode));  
    pc4->first.codop = LOAD;
    pc4->first.param.var = name(instattribute.node.fornode.rangvar);
    
    pc3->next = pc4;

    pc5 = (pseudocode)malloc(sizeof (struct pseudocodenode));    
    pc5->first.codop = JG;
    //label_num=myitoa(label_index++);   
    pc5->first.param.label_name = (char*) malloc(7+strlen(label_num));
    strcpy( pc5->first.param.label_name, "endfor");
    strcat( pc5->first.param.label_name, label_num);

    pc4->next = pc5;

    // le corps du for
    pc6 = generer_pseudo_code_list_inst(instattribute.node.fornode.forbodylinst, optimisationMode, associativityType);

    pc5->next = pc6;

    // l'incrémentation de l'indice

    pc7 = (pseudocode)malloc(sizeof (struct pseudocodenode));  
    pc7->first.codop = PUSH;
    setdvalue(&(pc7->first.param._const) , (double) 1);

    inserer_code_en_queue(pc6, pc7);

    pc8 = (pseudocode)malloc(sizeof (struct pseudocodenode));  
    pc8->first.codop = LOAD;
    pc8->first.param.var = name(instattribute.node.fornode.rangvar);
    
    pc7->next = pc8;

    pc9 = (pseudocode)malloc(sizeof (struct pseudocodenode));  
    pc9->first.codop = ADD;
    
    pc8->next = pc9;

    pc10 = (pseudocode)malloc(sizeof (struct pseudocodenode));  
    pc10->first.codop = STORE;
    pc10->first.param.var = name(instattribute.node.fornode.rangvar);

    pc9->next = pc10;

    // l'itération (le jmp au début)
    pc11 = (pseudocode)malloc(sizeof (struct pseudocodenode));    
    pc11->first.codop = JMP;
    pc11->first.param.label_name = (char*) malloc(strlen( pc2->first.param.label_name) + 1);
    strcpy( pc11->first.param.label_name, pc2->first.param.label_name);

    pc10->next = pc11;

    // la fin du for
    pc12 = (pseudocode)malloc(sizeof (struct pseudocodenode));    
    pc12->first.codop = LABEL;
    pc12->first.param.label_name = (char*) malloc(strlen( pc5->first.param.label_name)+1);
    strcpy( pc12->first.param.label_name,  pc5->first.param.label_name);
    pc12->next = NULL;

    pc11->next = pc12;

    break;
   
  }
  
  return pc;
}

pseudocode generer_pseudo_code_list_inst(listinstvalueType * plistinstattribute, OptimisationMode  optimisationMode, AssociativityType associativityType){
  pseudocode  pc1=NULL, pc2=NULL;
   if (debug) printf("in generer_pseudo_code_list_inst( )");
 if (plistinstattribute != NULL){
   pc1 = generer_pseudo_code_inst(plistinstattribute->first, optimisationMode, associativityType);    
   pc2 = generer_pseudo_code_list_inst(plistinstattribute->next, optimisationMode, associativityType);
   inserer_code_en_queue(pc1, pc2);
   if (debug) afficher_pseudo_code(pc1);
  }
   if (debug) printf("out generer_pseudo_code_list_inst( )");
 return pc1;
}

pseudocode generer_pseudo_code(listinstvalueType * plistinstattribute, OptimisationMode optimisationMode, AssociativityType associativityType){
	int i=0;
	pseudocode pcresult;
	pseudocode tete = NULL;
	//générer le code pour les DATA
	if (debug) printf("Generer le code pour les %d DATA...\n", nombre_variables());
	for(i = nombre_variables() - 1; i >= 0; i--){
		if (debug) printf("%d ieme malloc.....\n",i);
		pcresult = (pseudocode) malloc(sizeof(struct pseudocodenode));
		if (debug) printf("DATA..\n");
		pcresult->first.codop = DATA;
		if (debug) {printf("malloc..pour....\n"); printf("%s\n", name(i));}
		pcresult->first.param.nv.name = (char*) malloc(strlen(name(i))+1);
		if (debug) printf("strcpy..\n");
		strcpy(pcresult->first.param.nv.name, name(i));
		pcresult->first.param.nv.value = valinit(i);
		if ((debug) && (pcresult->first.param.nv.value.etype == DBL)) {printf("valinit == ..\n"); printf("%lf\n", dvalue(valinit(i)));}
		if ((debug) && (pcresult->first.param.nv.value.etype == STR)) {printf("valinit == ..\n"); printf("%s\n", svalue(valinit(i)));}				
		if (debug) printf("tete..\n");
		if (debug) {printf("utilisation == ..\n"); printf("%s\n", (est_utilisee(i)==true)?"true":"false");}
		pcresult->first.param.nv.utilisee = est_utilisee(i); // nécessaire pour l'optimisateur
		pcresult->next = tete;
		if (debug) printf("pcresult..\n");
		tete = pcresult;

	}

	// générer le begin:
	if (debug) printf("Generer le code pour le label begin...\n");
	pseudocode pcbegin = (pseudocode) malloc(sizeof(struct pseudocodenode));
	pcbegin->first.codop = LABEL;
	pcbegin->first.param.label_name = (char*) malloc(6);
	strcpy(pcbegin->first.param.label_name, "begin");
	pcbegin->next = NULL;

	//générer le code pour les INSTRUCTUTIONS
	if (debug) printf("Generer le code pour les instructions...\n");
	pseudocode pcbody = generer_pseudo_code_list_inst(plistinstattribute, optimisationMode, associativityType);

	// lier le label begin en début du body
	if (debug) printf("lier le label begin en début du body...\n");
	inserer_code_en_queue(pcbegin, pcbody);

	// générer le label end:
	if (debug) printf("générer le label end:...\n");
	pseudocode pcend = (pseudocode) malloc(sizeof(struct pseudocodenode));
	pcend->first.codop = LABEL;
	pcend->first.param.label_name = (char*) malloc(4);
	strcpy(pcend->first.param.label_name, "end");
	pcend->next = NULL;

	// lier le label end en fin du body
	if (debug) printf("lier le label end en fin du body...\n");
	inserer_code_en_queue(pcbody,pcend);

	// lier le tout begin-insts-end en fin des data
	if (debug) printf("lier le tout begin-insts-end en fin des data...\n");
	inserer_code_en_queue(pcresult,pcbegin);

	return pcresult;
}

void print_Optimisation_Mode( OptimisationMode _optimisationMode){
	switch( _optimisationMode ){
		case noOptimisation : printf(" noOptimisation "); break;
		case staticOptimisation : printf(" staticOptimisation "); break;
		case dynamicOptimisation : printf(" dynamicOptimisation "); break;
	}
}

void print_Associativity( AssociativityType _associativityType ){
	switch( _associativityType ){
		case leftAssociativity  : printf(" leftAssociativity "); break;
		case rightAssociativity : printf(" rightAssociativity "); break;
	}
}
