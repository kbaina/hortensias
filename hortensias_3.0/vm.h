#ifndef VM_H
#define VM_H

#include "pseudocode.h"

// @auteur Karim Baïna, ENSIAS, Décembre 2010, mis à jour Décembre 2018, puis Octobre 2019

// (analysis (e.g. division by zero detection), optimisation (e.g. dead path elimination), interpretation)
typedef enum {analysis=0, optimisation=1, interpretation=2} pseudocodeVisitMode;

// initialise la machine abstraite
void initialiser_machine_abstraite();

// visite une pseudo insctruction  avec trois modes (codeanalysis (e.g. division by zero detection), optimisation (e.g. dead path elimination), interpretation)
// cas codeanalysis : comme le mode optimisation sauf qu'il ne fait pas de marquage de la pseudo instruction
// cas optimisation : simule l'exécution et effectue l'opération de la pseudo instruction avec les vraies valeur simulées en mémoire statique et en pile, effectue le marquage de la pseudo instruction, et ne fait pas d'affichage du résultat de la pseudo instruction
// cas interpretation : effectue l'interprétation de la pseudo instruction
void visiter_pseudo_instruction(struct pseudoinstruction *ppi, char ** next_label_name, pseudocodeVisitMode pvm);

// precondition pc <> NULL
// interprete un pseudocode
// le résultat est en tête de pile.
void interpreter_pseudo_code(pseudocode pc);

// precondition pc <> NULL
// precondition le pseudo-code se termine en un temps fini
// optimise un pseudocode en éliminant les dead paths
void optimiser_pseudo_code(pseudocode pc);

// precondition pc <> NULL
// precondition le pseudo-code se termine en un temps fini
// visite le pseudocode et détecte les anomalies de type (division by zero)
void analyser_pseudo_code(pseudocode pc);

// precondition pc <> NULL
// visite un pseudocode selon trois modes (codeanalysis (e.g. division by zero detection), optimisation (e.g. dead path elimination), interpretation)
// cas codeanalysis : comme le mode optimisation sauf qu'il ne fait pas de marquage du pseudo code
// cas optimisation : simule l'exécution du pseudo code avec les vraies valeur simulées en mémoire statique et en pile, effectue le marquage du pseudo code, et ne fait pas d'affichage du résultat du pseudo code
// cas interpretation : effectue l'interprétation du pseudo code
void visiter_pseudo_code(pseudocode pc, pseudocodeVisitMode pvm);


#endif

