#include <stdio.h>
#include <string.h>
#include "analyseur_hortensias.h"
#include "tablesymb.h"
#include "cfg.h"
#include "error.h"
#include "pseudocode.h"
#include "vm.h"
#include "i18n.h"

#include <stdlib.h>

#define debug false

// @auteur Karim Baïna, ENSIAS, Décembre 2010, puis Décembre 2011, puis Décembre 2012 et corrigé en Décembre 2013 puis updated en Décembre 2018


// gérer l'associatvité gauche des opérateurs binaires (AST gauches) avec une grammaire récursive non gauche (LL(1))

/* 
 PRE_PROG : LISTE_PRAGMA PROG
 LISTE_PRAGMA : PRAGMA LISTE_PRAGMA | epsilon
 PROG : LISTE_DECL begin LISTE_INST end
 LISTE_INST : INST LISTE_INSTAUX
 LISTE_INSTAUX : LISTE_INST  | epsilon
 LISTE_DECL : DECL LISTE_DECLAUX 
 LISTE_DECLAUX : LISTE_DECLAUX : LISTE_DECL | epsilon
 DECL : idf TYPE DECL_AUX // Il faut une liste_idf type ex. value, a,b,c,d,e,f,g double;
 DECL_AUX : CONST ';' | ';'
 TYPE : TYPE : int | bool | double | string 
 CONST : inumber | dnumber | cstring | true | false
 INST : INST : idf = ADDSUB | true | false ';' 
      | if ‘(‘ idf == ADDSUB ‘)’ then LISTE_INST IF_INSTAUX 
      | 
print IDF ';' | print CSTRING ;
      | for IDF = dnumber to dnumber do LISTE_INST endfor
      | switch ( IDF ) SWITCH_BODY default : ... break ; endswitch 

 SWITCH_BODY : case inumber : LIST_INST break ;  SWITCH_BODYAUX

 SWITCH_BODYAUX : SWITCH_BODY | epsilon

 IF_INSTAUX :  endif  | else LISTE_INST endif

 ADDSUB : MULTDIV ADDSUBAUX

 ADDSUBAUX : – MULTDIV ADDSUBAUX
 ADDSUBAUX : + MULTDIV ADDSUBAUX
 ADDSUBAUX : epsilon

 MULTDIV : AUX MULTDIVAUX

 MULTDIVAUX : * AUX MULTDIVAUX ==> * MULTDIV
 MULTDIVAUX : / AUX MULTDIVAUX ==> / MULTDIV
 MULTDIVAUX : epsilon

 AUX : inumber | dnumber
 AUX : ( ADDSUB )

*/

static OptimisationMode optimisationMode = noOptimisation ; // by default the optimizer is not ON [generate left AST child first when leftoptimiiativity. generate left AST child first when rightAssociativity. other basic optimisations : transform not modified variable to constant, etc.]

static AssociativityType associativityType = leftAssociativity; // by default the semantic analyser takes leftAssociativity for +, -, *, /

static boolean verboseMode = false;


boolean _pre_prog(listinstvalueType ** pplistinstattribute);

boolean _liste_pragma();


boolean _prog(listinstvalueType ** pplistinstattribute);

boolean _liste_inst(listinstvalueType ** pplistinstattribute);
boolean _liste_inst_aux(listinstvalueType ** pplistinstattribute);
boolean _inst(instvalueType ** ppinstattribute);

boolean _switch_body();
boolean _switch_body_aux();

boolean _if_inst_aux(listinstvalueType ** pplistinstattribute);

boolean _liste_decl();
boolean _liste_decl_aux();
boolean _decl();
boolean _decl_aux();
boolean _type();
boolean _const();

boolean _addsub(AST *past);
boolean _addsubaux(AST *past);
boolean _multdiv(AST *past);
boolean _multdivaux(AST *past);
boolean _aux(AST *past);

//void _yyless(char *tokenimage); depricated : remet la chaîne paramètre à l'entrée standard

typetoken _lire_token();

extern int yylex();

typetoken token;
 
boolean follow_token = false;

varvalueType varattribute;
constvalueType constattribute;
typevalueType typeattribute;
instvalueType instattribute;
listinstvalueType listinstattribute;
tokenvalueType tokenattribute;
stringvalueType stringattribute;

int rangvar;
boolean semanticerror = false;

int main(int argc, char ** argv){
	listinstvalueType ** pplistinstattribute = (listinstvalueType **) malloc (sizeof(listinstvalueType *));
	*pplistinstattribute = NULL;

	if (argc == 2)
		{if (strcmp(argv[1],"-v")==0) printf("%s %s\n",dictionary[MessageDeBienvenueC][get_LANGUE_COURANTE()], VERSION);}
 	else{
	set_token_attributes ( 1 ); // line is equals to 1 at the beginning : invariant [yylineno == tokenattribute.line]
	set_Optimisation_Mode( noOptimisation ); // par défaut le mode optimiser est sur OFF
	token = _lire_token();
	if (_pre_prog(pplistinstattribute) == true) {
		if (debug) ("0 erreurs syntaxiques\n");

		if (nombre_sm_erreurs() == 0){
			if (debug) {if (semanticerror == true)  printf("BUGG 0 semantic error BUT semanticerror == true !!\n");}
			if (debug) printf("0 erreurs sémantiques\n");
			if (debug) afficherTS();

			if (verboseMode == true) {
				printf("%s %s\n",dictionary[MessageDeBienvenueC][get_LANGUE_COURANTE()], VERSION);
				printf("%s : \n", dictionary[AffichageControlFlowGraph][get_LANGUE_COURANTE()]); afficher_list_inst(*pplistinstattribute);
			}else{
				if (debug) printf("Generation du code ...\n");			
				pseudocode pc = generer_pseudo_code(*pplistinstattribute, optimisationMode, associativityType);
	
				if (debug) { printf("Affichage du pseudocode avant optimisation :\n");}
			
				pseudocodeVisitMode pvm ;
				initialiser_machine_abstraite();
				switch ( get_Optimisation_Mode ( ) )	{ // either dynamic or static
	
					case noOptimisation : 
						      analyser_pseudo_code( pc ); // detection de division par zéro sans marquage
							if (nombre_sm_erreurs() != 0){
								afficher_sm_erreurs();  // we detect divison by zero in this mode
							}else{
							      if (debug) printf("Affichage du pseudocode après optimisation :\n");
							      afficher_pseudo_code( pc );	
							}
						      break;
					default :    // staticOptimiser or DynamicOptimiser :  detection de division par zéro et marquage
						      optimiser_pseudo_code( pc ); // side effect on pc which is in R/W (by pointer)
							if (nombre_sm_erreurs() != 0){
								afficher_sm_erreurs();  // we detect divison by zero in this mode
							}else{
							      if (debug) printf("Affichage du pseudocode après optimisation :\n");
							      afficher_pseudo_code_optimise( pc );	
							}
						      break;
				}
				
			}
		}else{
			if (debug) afficherTS();
			//printf("%d erreurs sémantiques\n", nombre_sm_erreurs());
			afficher_sm_erreurs();
		}
	}else {
		/* printf("%d erreurs syntaxiques\n", nombre_sx_erreurs());*/
		if (nombre_sx_erreurs() == 0) creer_sx_erreur(NonCategorized, tokenattribute.line);
	        // else printf("erreurs syntaxiques :\n");
		afficher_erreurs();  // toutes les erreurs syntaxiques ne sont pas catégorisées et stockées
		if (debug) afficherTS();
			
		//printf("%d erreurs sémantiques\n", nombre_sm_erreurs());
		//if (nombre_sm_erreurs()> 0) afficher_sm_erreurs();
	}
	}
	
return 0;
}

//  PRE_PROG : LISTE_PRAGMA PROG

boolean _pre_prog(listinstvalueType ** pplistinstattribute){
	boolean result;
	if (debug) printf("pre_prog()\n");

	if (_liste_pragma()) {
		token = _lire_token();
		if (_prog(pplistinstattribute)){
			result = true;
		}else{
			result = false;		
		}
	}else{
		result = false;		
	}
	if (debug) printf("out of pre_prog()\n");

	return result;
}

// LISTE_PRAGMA : PRAGMA LISTE_PRAGMA | epsilon
// NULLABLE(LISTE_PRAGMA) = true
// follow(LISTE_PRAGMA) = {idf}
// first(LISTE_PRAGMA) = {pragma}

boolean _liste_pragma(){
	boolean result;
	if (debug) printf("liste_pragma()\n");

	if (token == IDF) {
		follow_token = true;
		result = true;
	}else if (token == PRAGMA){
		token = _lire_token();
		if (_liste_pragma()){
			result = true;
		}else{
			result = false;		
		}
	}else{
		result = false;
		creer_sx_erreur(PragmaOrIdfExpected, tokenattribute.line);
	}
		
	if (debug) printf("out of liste_pragma()\n");

	return result;
}


// PROG : LISTE_DECL begin LISTE_INST end
boolean _prog(listinstvalueType ** pplistinstattribute){
	boolean result;
	if (debug) printf("prog()\n");

	if (_liste_decl()) {
		token = _lire_token();
		if (token == BEG_IN){
			token = _lire_token();
			if (_liste_inst(pplistinstattribute)) {
				token = _lire_token();
				if (token == END){
					result = true;
				} else {
					result = false;
					creer_sx_erreur(EndExpected, tokenattribute.line);
				}
			}else result = false;
		}else{
			creer_sx_erreur(BeginExpected, tokenattribute.line);
			result = false;
		}
	}else result = false;
	
	if (debug) printf("out of prog()\n");
	return result;
}

// LISTE_INST : INST LISTE_INSTAUX
boolean _liste_inst(listinstvalueType ** pplistinstattribute){
	boolean result;
	if (debug) printf("liste_inst()\n");

	instvalueType ** ppinstattribute = (instvalueType **) malloc(sizeof(instvalueType *));
	*ppinstattribute = NULL;

	if (debug) printf("avant 	if (_inst(ppinstattribute)) {\n");
	if (_inst(ppinstattribute)) {
		// bugg !! les ast ne sont pas termines printf("new inst ===>");afficher_inst(**ppinstattribute);
		token = _lire_token();
		
		if (_liste_inst_aux(pplistinstattribute) == true){
			if (debug) printf("avant 	(semanticerror != true) inserer_inst_en_tete(pplistinstattribute, **ppinstattribute);\n");
			if (semanticerror != true) {
				if (debug) printf("avant inserer_inst_en_tete()!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
				if (*ppinstattribute == NULL) printf("BUGGGGGG  NUUUUUUUUUUUUUUUUUUUUUUULLLLLLLLLLLLLLLLLLLLL %d\n", semanticerror);
				inserer_inst_en_tete(pplistinstattribute, **ppinstattribute);
				if (debug) printf("apres inserer_inst_en_tete()!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
			}
			result = true;
		}else result = false;
	}else result = false;

	if (debug) printf("out of liste_inst()\n");
	return result;
}

// LISTE_INSTAUX : LISTE_INST  | epsilon
// NULLABLE(LISTE_INSTAUX) = true
// follow(LISTE_INSTAUX) = {end, endif, else, endfor, break}
// first(LISTE_INSTAUX) = {idf}
boolean _liste_inst_aux(listinstvalueType ** pplistinstattribute){
        boolean result;
	if (debug) printf("liste_inst_aux()\n");

	if (token == END){
		follow_token = true;
		* pplistinstattribute = NULL;
		result = true;
	} else if (token == ENDIF){
		follow_token = true;
		* pplistinstattribute = NULL;
		result = true;
	} else if (token == ELSE){
		follow_token = true;
		* pplistinstattribute = NULL;
		result = true;
	} else if (token == ENDFOR){
		follow_token = true;
		* pplistinstattribute = NULL;
		result = true;
	}  else if (token == BREAK){
		follow_token = true;
		* pplistinstattribute = NULL;
		result = true;
	} else if (_liste_inst(pplistinstattribute) == true){
		result = true;
	} else result = false; // on ne gère pas les erreurs liés aux follows dans le nullable, mais dans son appelant

	if (debug) printf("out of liste_inst_aux()\n");	
	return result;
}

//  INST : idf = ADDSUB ';'
//         | idf = TRUE ';'
//         | idf = FALSE ';'
//         | if ‘(‘ idf == ADDSUB ‘)’ then LISTE_INST IF_INSTAUX
//         | print IDF ';'
//         | for IDF = dnumber to dnumber do LISTE_INST endfor
//         | switch ( IDF ) SWITCH_BODY default : ... break ; endswitch 

boolean _inst(instvalueType ** ppinstattribute){
	boolean result;
	if (debug) printf("inst()\n");

	AST *past = (AST *) malloc(sizeof(AST));
	(*past) = (AST) malloc(sizeof(struct Exp));
	listinstvalueType ** pplistthen = (listinstvalueType **) malloc (sizeof(listinstvalueType *));
	listinstvalueType ** pplistelse = (listinstvalueType **) malloc (sizeof(listinstvalueType *));
	listinstvalueType ** pplistfor = (listinstvalueType **) malloc (sizeof(listinstvalueType *));
	listinstvalueType ** pplistswitchdefaultbody = (listinstvalueType **) malloc (sizeof(listinstvalueType *));
	casevaluelinstType **cases = (casevaluelinstType **) malloc(sizeof(casevaluelinstType *));

	*pplistthen = NULL;
	*pplistelse = NULL;
	*pplistfor = NULL;
	*cases = NULL;
	*pplistswitchdefaultbody = NULL;

	* ppinstattribute = NULL;

	int localrangvar; constvalueType borneinfconstattribute, bornesupconstattribute, localconstattribute; varvalueType localvarattribute;
	
//         | switch ( IDF ) SWITCH_BODY default : ... break ; endswitch 

	if (token == SWITCH){
		token = _lire_token();
		if (token == POPEN){
			token = _lire_token();
			if (token == IDF){
				//boolean leftexpnotdeclaredsemanticerror = false;
				localvarattribute = varattribute;
				// 1ere gestion erreur NotDeclared : l'IDF (lexp) peut ne pas avoir été déclaré
				if (inTS(localvarattribute.name, &localrangvar) == false){
					if (debug) {printf("%sn'y est pas\n ", localvarattribute.name); afficherTS();}
					semanticerror = true ;
					//leftexpnotdeclaredsemanticerror = true;
					// leftexpnotdeclaredsemanticerror <> semanticerror qui peut être affectée par le rexp après l'appel de _addsub(past) ** aussi !!
					//(leftexpnotdeclaredsemanticerror == true) ==> semanticerror = true) (mais pas l'inverse)
					creer_sm_erreur(NotDeclared, localvarattribute.line, localvarattribute.name);
				}else if (typevar(localrangvar) != Int) {
					semanticerror = true ;
					// 2eme gestion erreur IncompatibleAssignType : l'affectation peut être mal typée
					creer_sm_erreur(IncompatibleForSwitch, varattribute.line, varattribute.name);
				}
				token = _lire_token();
				if (token == PCLOSE){
					token = _lire_token();
					int nbcases = 0;
					if (_switch_body( cases, &nbcases )){
						token = _lire_token();
						if (token == DEFAULT){
							token = _lire_token();
							if (token == DEUXPOINT){
								token = _lire_token();
								if (_liste_inst( pplistswitchdefaultbody )){
									token = _lire_token();
									if (token == BREAK){
										token = _lire_token();
										if (token == PVIRG){
											token = _lire_token();
											if (token == ENDSWITCH){
												result = true ;
												if (semanticerror != true)
												*ppinstattribute = creer_instruction_switch(localrangvar, nbcases, *cases,  *pplistswitchdefaultbody);
												utiliser ( localrangvar ); // necessaire pour l'optimisateur pour ne générer que les var utilisées
												modifier ( localrangvar ); // nécessaire pour l'optimisateur LOAD (variable) en PUSH (constante)	
											} else  {result = false; creer_sx_erreur(EndswitchExpected, tokenattribute.line);}
										} else  {result = false; creer_sx_erreur(PvirgExpected, tokenattribute.line);}
									} else  {result = false; creer_sx_erreur(BreakExpected, tokenattribute.line);}
								}else result = false;
							} else  {result = false; creer_sx_erreur(DeuxpointExpected, tokenattribute.line);}
						} else  {result = false; creer_sx_erreur(DefaultExpected, tokenattribute.line);}
					}else result = false;
				} else  {result = false; creer_sx_erreur(PcloseExpected, tokenattribute.line);}
			} else  {result = false; creer_sx_erreur(IdfExpected, tokenattribute.line);}
		} else  {result = false; creer_sx_erreur(PopenExpected, tokenattribute.line);}
	}else if (token == FOR){ // for IDF = inumber to inumber do LISTE_INST endfor
		token = _lire_token();
		if (token == IDF){
			boolean leftexpnotdeclaredsemanticerror = false;
			
			if (inTS(varattribute.name, &localrangvar) == false) {
					semanticerror = true ;
					leftexpnotdeclaredsemanticerror = true;
					creer_sm_erreur(NotDeclared, varattribute.line, varattribute.name);
			} else if (typevar(localrangvar) != Int) {
					semanticerror = true ;
					// 2eme gestion erreur IncompatibleAssignType : l'affectation peut être mal typée
					creer_sm_erreur(IncompatibleForIndexType, varattribute.line, varattribute.name);
			} 
			token = _lire_token();
			if (token == EQ) {
				token = _lire_token();
				if (token == INUMBER) {
					if (leftexpnotdeclaredsemanticerror == false) { definir( localrangvar );}
					borneinfconstattribute = constattribute;
					token = _lire_token();
					if (token == TO) {
						token = _lire_token();
						if (token == INUMBER) {
							bornesupconstattribute = constattribute;			
							token = _lire_token();
							if (token == DO) {
								token = _lire_token();
								if (_liste_inst(pplistfor)) {
									token = _lire_token();
									if (token == ENDFOR){
										result = true;
										if (semanticerror != true)
					*ppinstattribute = creer_instruction_for(localrangvar, borneinfconstattribute.valinit, bornesupconstattribute.valinit,*pplistfor);
					utiliser ( localrangvar ); // necessaire pour l'optimisateur pour ne générer que les var utilisées
					modifier ( localrangvar ); // nécessaire pour l'optimisateur LOAD (variable) en PUSH (constante)					
									} else {result = false; creer_sx_erreur(EndforExpected, tokenattribute.line); }
								}else result = false;
							}else {result = false; creer_sx_erreur(DoExpected, tokenattribute.line);}
						}else {result = false; creer_sx_erreur(InumberExpected, tokenattribute.line);}
					}else { result = false; creer_sx_erreur(ToExpected, tokenattribute.line);}
				}else { result = false; creer_sx_erreur(InumberExpected, tokenattribute.line);}
			}else { result = false; creer_sx_erreur(EqExpected, tokenattribute.line);}
		}else { result = false; creer_sx_erreur(IdfExpected, tokenattribute.line);}
	}else if (token == PRINT){ // print X (X of int, double or string type)
		token = _lire_token();
		if (token == IDF){
			if (inTS(varattribute.name, &rangvar) == false) {
					semanticerror = true ;
					creer_sm_erreur(NotDeclared, varattribute.line, varattribute.name);
			}else{
				if (semanticerror != true)    {
					*ppinstattribute = creer_instruction_printIdf(rangvar);
					utiliser ( rangvar ); // necessaire pour l'optimisateur pour ne générer que les var utilisées
				}
			}
			token = _lire_token();
			if (token == PVIRG) {
				result = true;
			}else {result = false; creer_sx_erreur(PvirgExpected, tokenattribute.line);}
		}else if (token == CSTRING){ // print "..some const string..."
			*ppinstattribute = creer_instruction_printString( stringattribute.value );
			token = _lire_token();
			if (token == PVIRG) {
				result = true;
			}else {result = false; creer_sx_erreur(PvirgExpected, tokenattribute.line);}
		}else {result = false; creer_sx_erreur(CStringExpected, tokenattribute.line);}
	}else if (token == IDF){
		boolean leftexpnotdeclaredsemanticerror = false;
		localvarattribute = varattribute;
		// 1ere gestion erreur NotDeclared : l'IDF (lexp) peut ne pas avoir été déclaré
		if (inTS(localvarattribute.name, &localrangvar) == false){
					if (debug) {printf("%sn'y est pas\n ", localvarattribute.name); afficherTS();}
					semanticerror = true ;
					leftexpnotdeclaredsemanticerror = true;
					// leftexpnotdeclaredsemanticerror <> semanticerror qui peut être affectée par le rexp après l'appel de _addsub(past) ** aussi !!
					//(leftexpnotdeclaredsemanticerror == true) ==> semanticerror = true) (mais pas l'inverse)
					creer_sm_erreur(NotDeclared, localvarattribute.line, localvarattribute.name);
                }
		token = _lire_token();
		if (token == EQ){
			token = _lire_token();
			if (token == TRUE){
				token = _lire_token();
				if (token == PVIRG){
				     if (leftexpnotdeclaredsemanticerror != true){
				     	if (typevar(localrangvar) == Bool) {
						*past = creer_feuille_booleen(true);
						*ppinstattribute = creer_instruction_affectation(localrangvar, past);
						utiliser ( localrangvar ); // necessaire pour l'optimisateur pour ne générer que les var utilisées
						modifier ( localrangvar ); // nécessaire pour l'optimisateur LOAD (variable) en PUSH (constante)
					     }else{
						semanticerror = true ;
						// 2eme gestion erreur IncompatibleAssignType : l'affectation peut être mal typée
						creer_sm_erreur(IncompatibleAssignType, localvarattribute.line, localvarattribute.name);
					     }
				     }
				     result = true;
				}else{
					result =  false;
					creer_sx_erreur(PvirgExpected, tokenattribute.line);
				}
			}else if (token == FALSE){
				token = _lire_token();
				if (token == PVIRG){
				     if (leftexpnotdeclaredsemanticerror != true){
					     if (typevar(localrangvar) == Bool){
						*past = creer_feuille_booleen(false);
						*ppinstattribute = creer_instruction_affectation(localrangvar, past);
						utiliser ( localrangvar ); // necessaire pour l'optimisateur pour ne générer que les var utilisées
						modifier ( localrangvar ); // nécessaire pour l'optimisateur LOAD (variable) en PUSH (constante)
					     }else{
						semanticerror = true ;
						// 2eme gestion erreur IncompatibleAssignType : l'affectation peut être mal typée
						creer_sm_erreur(IncompatibleAssignType, localvarattribute.line, localvarattribute.name);
					     }
				     }
				     result = true;
				}else{
					result =  false;
					creer_sx_erreur(PvirgExpected, tokenattribute.line);
				}
			}else if (_addsub(past)){ // (**) leftexpdeclared <> !semanticerror qui peut être affectée par le rexp après l'appel de _addsub(past) ** aussi !!
				// Ce traitement n'a de sens que si la variable a bien été déclarée !!
				if ((debug) && (*past == NULL))  printf("BUGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG NUUUUUUUUUUULLLLLLLLLL *past !!!!!");
				if (debug) printf("HNA\n"); 
				if (leftexpnotdeclaredsemanticerror == true) {afficher_sm_erreurs(); printf("Cause leftexpnotdeclaredsemanticerror !!!\n");} 

				if (debug) { printf("{ ");afficher_infixe_arbre( *past ); printf(" }\n");}
				if (debug) { printf("name(%d):%s = {",localrangvar, name(localrangvar));afficher_infixe_arbre( *past ); printf(" }\n");}
				if (leftexpnotdeclaredsemanticerror != true) {
					if ( typevar(localrangvar) != type(*past) ) { // Double/Int, Int/Double, Bool/Int, Bool/Double
						if (debug) printf("ICI\n");
						if (debug)  printf("typelexp(%s) == %s\n",name(localrangvar),(typevar(localrangvar)==Int)?"Int":((typevar(localrangvar)==Double)?"Double":"Boolean"));
						if (debug) printf("typerexp == %s\n",(type(*past)==Int)?"Int":((type(*past)==Double)?"Double":"Boolean"));
						if (debug) printf("LA\n");
					     if ( (typevar(localrangvar) == Double) && (type(*past) == Int)){
						if (debug) printf("LA BAS\n");
						(*past)->typename = Double; // Casting implicit Double = (Double) Int
					     }else{
						if (debug) printf("LA CI BAS\n");
						semanticerror = true ;
						// 2eme gestion erreur IncompatibleAssignType : l'affectation peut être mal typée
						creer_sm_erreur(IncompatibleAssignType, localvarattribute.line, localvarattribute.name);
					     }
					}
				}
				if (debug) printf("LA TRES BAS\n");
				token = _lire_token();
				if (token == PVIRG){
					// Ce traitement n'a de sens que si la variable a bien été déclarée !!
					if (semanticerror != true) {
						*ppinstattribute = creer_instruction_affectation(localrangvar, past);
						utiliser ( localrangvar ); // necessaire pour l'optimisateur
						modifier ( localrangvar ); // nécessaire pour l'optimisateur LOAD (variable) en PUSH (constante)
					}
					result = true;
				} else { result = false; creer_sx_erreur(PvirgExpected, tokenattribute.line);}
			} else {result = false;			creer_sx_erreur(TrueorFalseExpected, tokenattribute.line);} //_addsub() a affiché ses erreurs éventuelles, (token == TRUE) (token == FALSE) ont suspecté un true ou false et au moment de l'affichae on va regroupé par yylineno ;-) pour consolider les affichages liés à la même ligne ;-)
		} else result = false;
		if (leftexpnotdeclaredsemanticerror != true)  definir( localrangvar ); // quelque soit le cas (existence d'erreur sémantique hors NotDeclared causée par le leftexp) même si une variable n'a pas été initialisée, si elle a été définie par affectation cela nnule l'erreur NoInitilized
	}else if (token == IF){
		token = _lire_token();
		if (token == POPEN){
			token = _lire_token();
			boolean leftexpnotdeclaredsemanticerror = false;
			if (token == IDF){
				localvarattribute = varattribute;
				// 3eme gestion erreur NotDeclared : l'IDF (lexp) peut ne pas avoir été déclaré
				if (inTS(localvarattribute.name, &localrangvar) == false){
							semanticerror = true;		
							leftexpnotdeclaredsemanticerror = true;
						// leftexpnotdeclaredsemanticerror <> semanticerror qui peut être affectée par le rexp après l'appel de _addsub(past) ** aussi !!
						//(leftexpnotdeclaredsemanticerror == true) ==> semanticerror = true) (mais pas l'inverse)
							creer_sm_erreur(NotDeclared, localvarattribute.line, localvarattribute.name);
				}
				token = _lire_token();
				if (token == EQEQ){
					token = _lire_token();
					if (_addsub(past)){
						// N'a vraiement de sens que s'il n'y pas d'erreur sémantique dans la déclaration du IDF
						if (leftexpnotdeclaredsemanticerror != true) {
							if ( typevar(localrangvar) != type(*past) ){ //Int/Double, Double/Int, Double/Bool, Bool/Double, Int/Bool, Bool/Int
								// 4eme gestion erreur IncompatibleCompType : la conditionnelle peut être mal typée
								if ( ((typevar(localrangvar) != Double) && (typevar(localrangvar) != Int)) || 
                                                                     ((type(*past) != Double)           && (type(*past) != Int))) { 
								// Double/Bool, Bool/Double, Int/Bool, Bool/Int
								// Double/String, String/Double, Int/String, String/Int
								// Bool/String, String/Bool
									semanticerror = true;
									creer_sm_erreur(IncompatibleCompType, localvarattribute.line, localvarattribute.name);
								}// else casting implicite pas besoin de faire de traitement !! pour Int/Double, Double/Int
							}
						}
						token = _lire_token();
						if (token == PCLOSE){
							token = _lire_token();
							if (token == THEN){
								token = _lire_token();
								if (_liste_inst(pplistthen)){
									token = _lire_token();
									if (_if_inst_aux(pplistelse) == true){
										// N'a de sens que s'il n'y pas d'erreur sémantique dans tout le IF !!
										if (semanticerror != true){
											*ppinstattribute = creer_instruction_if(localrangvar, past, *pplistthen, *pplistelse);
											utiliser ( localrangvar ); // necessaire pour l'optimisateur
										}															
										//if (debug) afficher_inst(**ppinstattribute);
										result = true;
									}else result = false;
								}else result = false;
							}else {result = false; creer_sx_erreur(ThenExpected, tokenattribute.line);} 
						}else  {result = false; creer_sx_erreur(PcloseExpected, tokenattribute.line);}
					}else result = false;
				} else  {result = false; creer_sx_erreur(EqeqExpected, tokenattribute.line);}
			} else  {result = false; creer_sx_erreur(IdfExpected, tokenattribute.line);}
		} else  {result = false; creer_sx_erreur(PopenExpected, tokenattribute.line);}
	} else  {result = false; creer_sx_erreur(InstructionExpected, tokenattribute.line);}


	if (debug) if ((semanticerror == false) && (*ppinstattribute == NULL)) printf(" (semanticerror == false) && (*ppinstattribute == NULL) Bugg ou erreur syntaxique !!!\n"); // 
	if (debug) if ((semanticerror == true) && (*ppinstattribute != NULL)) printf("BUG!! (semanticerror == true) && (*ppinstattribute != NULL) !!!\n");

	if (debug) printf("out of inst()\n"); 
	return result;
}

// SWITCH_BODY : case inumber ':' LIST_INST break ';'  SWITCH_BODYAUX
boolean _switch_body(casevaluelinstType **cases, int * caseindex){

/*
 struct {
	int rangvar; // indice de la variable du switch
	struct CASE_BODY *cases ; // pour les cases (SWITCH_BODY), tableau dynamique non trié de couples val- liste
	struct LIST_INST * defaultbodylinst ; // la liste d'instructions par défaut du switch
    } switchnode ;

typedef struct CASE_BODY {
	int value ; // la valeur du cas (doit être >= 0)
	struct LIST_INST * casebodylinst; // la liste d'instructions du cas
} casevaluelinstType;
*/
	boolean result;
	if (debug) printf("_switch_body()\n");

	if (caseindex == 0) 	*cases = (casevaluelinstType *) malloc (sizeof(casevaluelinstType) * (*caseindex + 1));
	else *cases = (casevaluelinstType *) realloc (*cases, sizeof(casevaluelinstType) * ((*caseindex) + 1));

	if (token == CASE){
		token = _lire_token();
		if (token == INUMBER){
			if (inPreviousCases(*cases, *caseindex, constattribute.valinit)){
				semanticerror = true;
				creer_sm_erreur(MultipleValueForSwitch, tokenattribute.line, varattribute.name);
			}else {
				(*cases)[ *caseindex ].value = constattribute.valinit;
			}
			token = _lire_token();
			if (token == DEUXPOINT){
				token = _lire_token();
				if (_liste_inst( & ((*cases)[*caseindex].casebodylinst) )){
					token = _lire_token();
					if (token == BREAK){
						token = _lire_token();
						if (token == PVIRG){
							token = _lire_token();
							(*caseindex)++; 
							if (_switch_body_aux( cases, caseindex)){
								result = true ;
							} else  result = false;
						} else  {result = false; creer_sx_erreur(PvirgExpected, tokenattribute.line);}
					} else  {result = false; creer_sx_erreur(BreakExpected, tokenattribute.line);}
				}else result = false;
			} else  {result = false; creer_sx_erreur(DeuxpointExpected, tokenattribute.line);}
		} else  {result = false; creer_sx_erreur(InumberExpected, tokenattribute.line);}
	} else  {result = false; creer_sx_erreur(CaseExpected, tokenattribute.line);}

	if (debug) printf("out of _switch_body()\n");

	return result;
}
// nullable (SWITCH_BODYAUX) = true
// follow(SWITCH_BODYAUX) = follow(SWITCH_BODYAUX)={default}
// first(SWITCH_BODYAUX) = {idf}

// SWITCH_BODYAUX : SWITCH_BODY | epsilon
boolean _switch_body_aux(casevaluelinstType **cases, int *caseindex){
	boolean result;


	if (debug) printf("_switch_body()\n");

	if (token == DEFAULT){
		follow_token = true;
		result = true;
	}else{
		result = _switch_body( cases, caseindex );
	}
 
	if (debug) printf("out of _switch_body()\n");

	return result;
}


// IF_INSTAUX :  endif | else LISTE_INST endif
boolean _if_inst_aux(listinstvalueType ** pplistinstattribute){
	boolean result;
	if (debug) printf("if_inst_aux()\n");

	* pplistinstattribute = NULL;

	if (token == ENDIF){
		result = true;
	}else if (token == ELSE){
		token = _lire_token();
		if (_liste_inst(pplistinstattribute) == true){
			token = _lire_token();
			if (token == ENDIF){
				result = true;			
			}else   {result = false; creer_sx_erreur(EndifExpected, tokenattribute.line);}
		} else result = false; 
	} else   {result = false; creer_sx_erreur(EndiforelseExpected, tokenattribute.line);}


	if (debug) printf("out of if_inst_aux()\n");
	return result;
}

// LISTE_DECL : DECL LISTE_DECLAUX
boolean _liste_decl(){
	boolean result;
	if (debug) printf("liste_decl()\n");

	if (_decl()){
		token = _lire_token();
		result = _liste_decl_aux();
	}else result = false;

	if (debug) printf("out of liste_decl()\n");
	return result;
}

// LISTE_DECLAUX : LISTE_DECL | '.' ==> LISTE_DECLAUX : LISTE_DECL | epsilon
// NULLABLE(LISTE_DECLAUX) = true
// follow(LISTE_DECLAUX) = {begin}
// first(LISTE_DECLAUX) = {idf}
boolean _liste_decl_aux(){
	boolean result;
	if (debug) printf("liste_decl_aux()\n");

	if ( (token == BEG_IN) ){
		// _yyless("\tbegin\n");
		follow_token = true;
		result = true;
	}else if (_liste_decl()){
		result = true;
	}else{
		result = false;
	}

	if (debug) printf("out of liste_decl_aux()\n");
	return result;
}

/*prog()
	liste_decl()
		decl()
			VAR{s1}type()
			out of type()
			decl_aux()
			out of decl_aux()
*/

// DECL : idf TYPE DECL_AUX
boolean _decl() {
	boolean result;

	if (debug) printf("decl()\n");

	if ( (token == IDF) ) {
		if (debug) printf("VAR{%s}",varattribute.name);
		token = _lire_token();
		if (_type() == true){
			token = _lire_token();
			if (_decl_aux() == true){
				// 5eme gestion erreur AlreadyDeclared : l'IDF peut être déjà déclaré
				if (inTS(varattribute.name, &rangvar) == true) {
					semanticerror = true;
					creer_sm_erreur(AlreadyDeclared, varattribute.line, varattribute.name);
				}else{
					varvalueType newvar;
					newvar.nbdecl = 1;
			    		newvar.name = (char *)malloc(sizeof(char)*strlen(varattribute.name)+1);
					strcpy(newvar.name, varattribute.name);
					if (debug) printf("VAR{%s}-->NEW{%s}",varattribute.name, newvar.name);
					newvar.line = varattribute.line;
					newvar.initialisation = varattribute.initialisation; // l'initialisation est marquée par decl_aux dans varattribute
					newvar.utilisation = false; //toute variable est par défaut non utilisee
				        newvar.typevar = typeattribute.typename;
					switch(newvar.typevar){
						case String : setsvalue(&(newvar.valinit) , ((varattribute.initialisation == true)?stringattribute.value:"")); break;
						default : setdvalue(&(newvar.valinit) , ((varattribute.initialisation == true)?constattribute.valinit:0.0)); break;
					}
					ajouter_nouvelle_variable_a_TS(newvar);
				}

				// 6eme gestion erreur BadlyInitialised : l'IDF peut avoir été initialisé par une constante du mauvais type
				if (varattribute.initialisation == true){
				    if (constattribute.typename != typeattribute.typename){ //Int/Double|String, Double/Int|String, Double/Bool|String, Bool/Double|String, Int/Bool|String, Bool/Int|String, //String|Int/Double, String/Double|Int, String/Double|Bool, String/Bool|Double, String/Int|Bool, String/Bool|Int,
					if ( (typeattribute.typename != Double) || (constattribute.typename != Int) ){  // ce n'est pas Double := Int
						semanticerror = true;
						creer_sm_erreur(BadlyInitialised, varattribute.line, varattribute.name);
					}
				        //sinon (i.e Double := Int) alors il y a un casting implicit Double = (Double) Int ;-)
				    }
				}
	
				result = true;
			}else result = false;
		}else result = false; 
	} else {result = false; creer_sx_erreur(IdfExpected, tokenattribute.line);}

	if (debug) printf("out of decl()\n");
	return result;
}

// DECL_AUX : CONST ';' | ';'
boolean _decl_aux() {
	boolean result;
	if (debug) printf("decl_aux()\n");

	if ( (token == PVIRG) ) { 
		varattribute.initialisation = false;		
		result = true; 
	}else if (_const()) {
		token = _lire_token();
		if (token == PVIRG) {
			varattribute.initialisation = true;		
			result = true;
		} else {result = false; creer_sx_erreur(PvirgExpected, tokenattribute.line);}
	}else {result = false; creer_sx_erreur(PvirgExpected, tokenattribute.line);} // const() a affiché ses erreurs éventuels,  (token == PVIRG) a suspecté une virgule et au moment de l'affichae on va regroupé par yylineno ;-) pour consolider les affichages liés à la même ligne ;-)

	if (debug) printf("out of decl_aux()\n");
	return result;
}

//TYPE : int | bool | double | string 
boolean _type() {
	boolean result;
	if (debug) printf("type()\n");

	if (token == INT) {typeattribute.typename = Int; result = true;}
	else if (token == DOUBLE) {typeattribute.typename = Double; result = true;}
	else if (token == BOOL) {typeattribute.typename = Bool; result = true;}
	else if (token == STRING) {typeattribute.typename = String; result = true;}
	else {result = false; creer_sx_erreur(TypeExpected, tokenattribute.line);}

	if (debug) printf("out of type()\n");
	return result;
}

// CONST : number | true | false ; ==> CONST : inumber | dnumber | cstring | true | false ; 
boolean _const() {
	boolean result;
	if (debug) printf("const()\n");

	if (token == INUMBER) {
		constattribute.typename = Int;
		result = true;	
	}else if (token == DNUMBER) {
		constattribute.typename = Double;
		result = true;
	}else if (token == CSTRING) {
		constattribute.typename = String;
		result = true;
	} else if ( (token == TRUE) || (token == FALSE) ){
		constattribute.typename = Bool;
		result = true;	
	} else  {result = false; creer_sx_erreur(ConstExpected, tokenattribute.line);}

	if (debug) printf("out of const()\n");
	return result;
}

// ADDSUB : MULTDIV ADDSUBAUX
// reçoit un arbre [past] en paramètre avec un arbre droit NULL
// lui installe l'arbre reçu de Multidiv comme arbre droit
// le repasse à ADDSUBaux en paramètre
boolean _addsub(AST *past){
   boolean result;
   if (debug) printf("addsub()\n");

   if (associativityType == leftAssociativity){
 	if (debug) printf("begin addsub left associativity processing()\n");
	// reçoit un arbre [past] en paramètre avec un arbre droit NULL
	// lui installe l'arbre reçu de Multidiv comme arbre droit
	// le repasse à ADDSUBaux en paramètre

	AST *past1 = (AST *) malloc(sizeof(AST));
	AST *past2 = (AST *) malloc(sizeof(AST));

	(*past1) = (AST) malloc (sizeof(struct Exp));
        // past a un arbre droit NULL
	if (_multdiv(past1)){
		token = _lire_token();

		if ((*past)->noeud.op.expression_gauche == NULL) (*past) = *past1; // initialisation par le première feuille gauche.
		else (*past)->noeud.op.expression_droite = *past1; 

		if (_addsubaux(past) == true){ 
				if ((arbre_droit(*past) != NULL) && (arbre_gauche(*past) != NULL)) {
					if (type(arbre_gauche(*past)) == type(arbre_droit(*past))){ // Int/Int ou Double/Double
						(*past)->typename = type(arbre_gauche(*past));
					}else (*past)->typename = Double;
				}else {(*past) = *past1;} // ??

			result = true;
		}else result = false;
	}else result = false;
 	if (debug) printf("end addsub left associativity processing()\n");
   }else{// leftAssociativity == false
 	if (debug) printf("begin addsub right associativity processing()\n");
	// produit un arbre dégénéré droit (associativité droite aulieu de gauche)	
   	AST *past1 = (AST *) malloc(sizeof(AST));
	AST *past2 = (AST *) malloc(sizeof(AST));

	if (_multdiv(past1)){
		token = _lire_token();
		if (_addsubaux(past2) == true){
			if ( (*past1 != NULL) && (*past2 != NULL) ){
				char op = ((top(*past2)==plus)?'+':((top(*past2)==moins)?'-':((top(*past2)==mult)?'*':'/')));
				if (debug) printf("--------->type(*past1) ==%s\n",(type(*past1)==Int)?"Int":(type(*past1)==Double)?"Double":"Bool");
				if (debug) printf("--------->type(arbre_droit(*past2)) ==%s\n",(type(arbre_droit(*past2))==Int)?"Int":(type(arbre_droit(*past2))==Double)?"Double":"Bool");
				
					if ( (type(*past1) == type(arbre_droit(*past2))) ){ //Int/Int ou Double/Double
						*past = creer_noeud_operation(op, *past1, arbre_droit(*past2), type(*past1), tokenattribute.line); // 1ere inférence de type d'arbre arithmétique
					} else{ 
						*past = creer_noeud_operation(op, *past1, arbre_droit(*past2), Double, tokenattribute.line); // casting implicite Int[+-*/]Double ou Double[+-*/]Int ==> Double
					}		
				//}
			}else *past = *past1;
			result = true;
		}else result = false;
	}else result = false;
 	if (debug) printf("end addsub right associativity processing()\n");
   }

   if (debug) printf("out of addsub()\n");
   return result;
}

// ADDSUBAUX : + MULTDIV ADDSUBAUX | - MULTDIV ADDSUBAUX | epsilon
// eq à (ADDSUBAUX : + ADDSUB | - ADDSUB | epsilon)
// NULLABLE(ADDSUBAUX) = true
// follow(ADDSUBAUX) = { ';' , ')' }
// first(ADDSUBAUX) = { '-' , '+' }
boolean _addsubaux(AST *past){
   boolean result;
   if (debug) printf("addsubaux()\n");

   if (associativityType == leftAssociativity){
	if (debug) printf("begin addsubaux left associativity processing()\n");
	// reçoit un arbre [past] en paramètre
	// crée un nouvel arbre [newpast] dont il place la racine [+ ou -], place [past] à sa gauche avec un arbre droit NULL
	// le repasse à ADDSUB en paramètre

	if ( (token == PVIRG) || (token == PCLOSE) ){
		follow_token = true;
		result = true;
	}else if (token == PLUS) {
			token = _lire_token();
			*past = creer_noeud_operation('+', *past, NULL, type(*past), tokenattribute.line); // 2eme inférence de type d'arbre arithmétique
			if (_addsub(past) == true){
				result = true;
			}else result = false;
	} else if (token == MINUS) {
			token = _lire_token();
			*past = creer_noeud_operation('-', *past, NULL, type(*past), tokenattribute.line); // 2eme inférence de type d'arbre arithmétique
			if (_addsub(past) == true){
				result = true;
			}else result = false;
	} else {result = false; creer_sx_erreur(AddorsubExpected, tokenattribute.line);}
	// NE PAS GERER LES ERREURS DES FOLLOWS  (ici (token == PVIRG) || (token == PCLOSE) ) DANS LES NULLABLES CAR DECONNECTES DE LEUR CONTEXTE D'APPEL (LE FOLLOW ET l'UNION DES POSSIBILITES ==> MESSAGES ERREURS TROP VAGUES)) MAIS LES GERER DANS LES APPELANTS DES NULLABLES PLUS CONTEXTUALISES !!
	if (debug) printf("end addsubaux left associativity processing()\n");
   }else{ //(leftAssociativity == false){
	if (debug) printf("begin addsubaux right associativity processing()\n");
   	// produit un arbre dégénéré droit (associativité droite aulieu de gauche)
	*past = NULL;

	AST *past1 = (AST *) malloc(sizeof(AST));

	if ( (token == PVIRG) || (token == PCLOSE) ){
		follow_token = true;
		result = true;
	}else if (token == PLUS) {
			token = _lire_token();
			if (_addsub(past1) == true){
				if (*past1 != NULL)
					*past = creer_noeud_operation('+', NULL, *past1, type(*past1), tokenattribute.line); // 2eme inférence de type d'arbre arithmétique
				result = true;
			}else result = false;
	} else if (token == MINUS) {
			token = _lire_token();
			if (_addsub(past1) == true){
				if (*past1 != NULL)
					*past = creer_noeud_operation('-', NULL, *past1, type(*past1), tokenattribute.line); //3eme inférence de type d'arbre arithmétique
				result = true;
			}else result = false;
	} else result = false;
	if (debug) printf("end addsubaux right associativity processing()\n");
   }
 if (debug) printf("out of addsubaux()\n");
 return result;
}
/*
inst()
	addsub()
		multdiv()
			aux()
			out of aux()
			multdivaux()
				multdiv()
					aux()
					out of aux()
					multdivaux()
					out of multdivaux()
				out of multdiv()
			out of multdivaux()
*/

// MULTDIV : AUX MULTAUX
boolean _multdiv(AST *past){
   boolean result;
   if (debug) printf("multdiv()\n");

   if (associativityType == leftAssociativity){
	if (debug) printf("begin _multdiv left associativity processing()\n");
	// reçoit un arbre [past] en paramètre avec un arbre droit NULL
	// lui installe l'arbre reçu de Aux comme arbre droit
	// le repasse à Multdivaux en paramètre

	AST *past1 = (AST *) malloc(sizeof(AST));
	AST *past2 = (AST *) malloc(sizeof(AST));

        // NEW 3 : past a un arbre droit NULL
	(*past1) = (AST) malloc (sizeof(struct Exp));

	if (_aux(past1)){
		token = _lire_token();

		if ((*past)->noeud.op.expression_gauche == NULL) (*past) = *past1; // initialisation par le première feuille gauche.
		else (*past)->noeud.op.expression_droite = *past1;

		if (_multdivaux(past) == true){
				if ((arbre_droit(*past) != NULL) && (arbre_gauche(*past) != NULL)) {
					if (type(arbre_gauche(*past)) == type(arbre_droit(*past))){ // Int/Int ou Double/Double
						(*past)->typename = type(arbre_gauche(*past));
					}else {
						(*past)->typename = Double;
						if (debug) {printf("<multdivICI>\n");afficher_infixe_arbre(*past);printf("\n");}
					}
				}else {(*past) = *past1;} // ??
			result = true;
		}else result = false;
	}else result = false;
	/*
			aux()
			out of aux()
			multdivaux()
				multdiv()
					aux()
					out of aux()
					multdivaux()
					out of multdivaux()
				out of multdiv()
			out of multdivaux()

	*/
	if (debug) printf("end _multdiv left associativity processing()\n");
    }else{// (leftAssociativity == false){
	if (debug) printf("begin _multdiv right associativity processing()\n");
	// produit un arbre dégénéré droit (associativité droite aulieu de gauche)
	*past = NULL;

	AST *past1 = (AST *) malloc(sizeof(AST));
	AST *past2 = (AST *) malloc(sizeof(AST));

	if (_aux(past1)){
		token = _lire_token();
		if (_multdivaux(past2) == true){
			if ( (*past1 != NULL) && (*past2 != NULL) ){
				if (debug) printf("( (*past1 != NULL) && (*past2 != NULL) )\n");
				char op = ((top(*past2)==plus)?'+':((top(*past2)==moins)?'-':((top(*past2)==mult)?'*':'/')));
				if (type(*past1) == type(arbre_droit(*past2))){ //Int/Int ou Double/Double
					*past = creer_noeud_operation(op, *past1, arbre_droit(*past2), type(*past1), tokenattribute.line); // 4eme inférence de type
				} else {
					*past = creer_noeud_operation(op, *past1, arbre_droit(*past2), Double, tokenattribute.line); // casting implicite Int[+-*/]Double ou Double[+-*/]Int ==> Double
				}
			}else {
				*past = *past1;
			}
			result = true;
		}else result = false;
	}else result = false;
	if (debug) printf("end _multdiv right associativity processing()\n");
    }
    if (debug) printf("out of multdiv()\n");
    return result;
}

// MULTDIVAUX : * AUX MULTDIVAUX | / AUX MULTDIVAUX | epsilon
// eq à (MULTDIVAUX : * MULTDIV | / MULTDIV | epsilon)
// NULLABLE(MULTDIVAUX) = true
// follow(MULTDIVAUX) = { '+' , '-' , ';' , ')' }
// first(MULTDIVAUX) = { '*', '/' }
boolean _multdivaux(AST *past){
	boolean result;
	if (debug) printf("multdivaux()\n");

      if (associativityType == leftAssociativity){
	if (debug) printf("begin _multdivaux left associativity processing()\n");
	// reçoit un arbre [past] en paramètre
	// crée un nouvel arbre [newpast] dont il place la racine [/ ou *], place [past] à sa gauche avec un arbre droit NULL
	// le repasse à MultDiv en paramètre
	
	if ( (token == PLUS) || (token == MINUS) || (token == PVIRG) || (token == PCLOSE) ){
		follow_token = true;
		result = true;
	}else if (token == MULT) {
			token = _lire_token();
			*past = creer_noeud_operation('*', *past, NULL, type(*past), tokenattribute.line); // NEW 7 : 2eme inférence de type d'arbre arithmétique
			if (_multdiv(past)){
				result = true;
			}else result = false;
	} else if (token == DIV) {
			token = _lire_token();
			*past = creer_noeud_operation('/', *past, NULL, type(*past), tokenattribute.line); // NEW 8 : 2eme inférence de type d'arbre arithmétique
			if (_multdiv(past)){
				result = true;			
			}else result = false;
	} else {result = false; creer_sx_erreur(MultordivExpected, tokenattribute.line);} // NE PAS GERER LES ERREURS DES FOLLOWS  (ici (token == PLUS) || (token == MINUS) || (token == PVIRG) || (token == PCLOSE) ) DANS LES NULLABLES CAR DECONNECTES DE LEUR CONTEXTE D'APPEL (LE FOLLOW ET l'UNION DES POSSIBILITES ==> MESSAGES ERREURS TROP VAGUES)) creer_sx_erreur(MultordivorplusorminusorpvirgorpcloseExpected, yylineno);
	if (debug) printf("end _multdivaux left associativity processing()\n");
   }else{ //(leftAssociativity == false){	
	if (debug) printf("begin _multdivaux right associativity processing()\n");
	// produit un arbre dégénéré droit (associativité droite aulieu de gauche)
	*past = NULL;

	AST *past1 = (AST *) malloc(sizeof(AST));
	
	if ( (token == PLUS) || (token == MINUS) || (token == PVIRG) || (token == PCLOSE) ){
		follow_token = true;
		result = true;
	}else if (token == MULT) {
			token = _lire_token();
			if (_multdiv(past1)){
				if ( (*past1 != NULL) )
					*past = creer_noeud_operation('*', NULL, *past1, type(*past1), tokenattribute.line); // 5eme inférence de type d'arbre arithmétique
				result = true;
			}else result = false;
	} else if (token == DIV) {
			token = _lire_token();
			if (_multdiv(past1)){
				if ( (*past1 != NULL) )
					*past = creer_noeud_operation('/', NULL, *past1, type(*past1), tokenattribute.line); //6eme inférence de type d'arbre arithmétique
				result = true;			
			}else result = false;
	} else result = false;
	if (debug) printf("end _multdivaux right associativity processing()\n");
   }

   if (debug) printf("out of multdivaux()\n");
   return result;
}

// AUX : idf | inumber | dnumber | ( ADDSUB ) 
boolean _aux(AST *past){
	boolean result;


	if (debug) printf("aux()\n");

	if (associativityType == rightAssociativity) { *past = NULL; }

	if  (token == IDF) {
		// 7eme gestion erreur NotDeclared : l'IDF peut ne pas avoir été déclaré
		if (inTS(varattribute.name, &rangvar) == false) {
			semanticerror = true;
			creer_sm_erreur(NotDeclared, varattribute.line, varattribute.name);
		
		// 8eme gestion erreur IncompatibleOperationType : l'IDF peut avoir été déclaré d'un type non entier ni double
		}else if ( (typevar(rangvar) != Int) && (typevar(rangvar) != Double) ) { // (si l'IDF est un Bool)
			semanticerror = true;
			creer_sm_erreur(IncompatibleOperationType, varattribute.line, varattribute.name);
		}else if (est_definie(rangvar) == false){
			semanticerror = true;
			creer_sm_erreur(NotInitialized, varattribute.line, varattribute.name);		
			// l'IDF est Int ou Double mais n'est pas définie (permet de ne pas bloquer la machinerie de l'analyseur sémantique) 
			// surtout que la non définition n'est pas une erreur sémantique bloquante
			*past = 
creer_feuille_idf(name(rangvar), typevar(rangvar)); // On ne peut pas donc avoir un AST de type Bool (meme si l'IDF est un Bool)
		}else{
			// l'IDF est Int ou Double et est initialisee 
			*past = creer_feuille_idf(name(rangvar), typevar(rangvar)); // On ne peut pas donc avoir un AST de type Bool (meme si l'IDF est un Bool)
			utiliser ( rangvar ); //sert à l'optimisateur
		}
		result = true;
	} else if (token == INUMBER) {
		*past = creer_feuille_nombre(constattribute.valinit, Int);
		result = true;
	} else if (token == DNUMBER) {
		*past = creer_feuille_nombre(constattribute.valinit, Double);
		result = true;
	}  else if (token == POPEN) {
		token = _lire_token();
		if (_addsub(past)){
			token = _lire_token();
			if (token == PCLOSE){
				result = true;
			}else {
				result = false;
				creer_sx_erreur(PcloseExpected, tokenattribute.line);
			}
		}else result = false;
	} else  {result = false; creer_sx_erreur(IdforinumberordnumberorpopenExpected, tokenattribute.line);}


	if (debug) printf("out of aux()\n");
	return result;
}

// depricated : remet la chaîne paramètre (image du token courant) à l'entrée standard
/*void _yyless(char *tokenimage){
	int n = strlen(tokenimage) - 1;
	while (n >= 0)	{ungetc(tokenimage[n], stdin); n--;}
}*/

// lit le prochain token s'il n'a pas déjà été lu par le prédicat d'un nullable
typetoken _lire_token(){
	if (follow_token){
		follow_token = false;
		return token;
	}else{ 
		return (typetoken) yylex();
	}
}

void set_idf_attributes(char *name, int line){
	if (debug) printf("[%s]", name);
	varattribute.name = (char *)malloc(sizeof(char) * strlen(name)+1);
	strcpy(varattribute.name, name);
	if (debug) printf("[%s]", varattribute.name);
	varattribute.line = line;
}

void set_number_attributes(double value){
	constattribute.valinit = value;
}

void set_string_attributes(char * s){
	if (debug) printf("set_string_attributes()\n");
	if ((debug) && (s == NULL)) printf("s NULL!!!!!!!\n"); 

	stringattribute.value = (char *)malloc((strlen(s)+1) * sizeof(char));
	strcpy(stringattribute.value, s);
	if (debug) printf("out set_string_attributes()\n");
}



void set_Optimisation_Mode( OptimisationMode _optimisationMode ){
	optimisationMode = _optimisationMode;
}

OptimisationMode get_Optimisation_Mode( ){
	return optimisationMode;
}

void set_Associativity_Type( AssociativityType _associativityType ){
	associativityType = _associativityType;
}

AssociativityType get_Associativity_Type( ){
	return associativityType;
}


void set_Verbose_Mode( boolean _verboseMode ){
	verboseMode = _verboseMode;
}

boolean get_Verbose_Mode( ){
	return verboseMode;
}



void set_token_attributes(int line){
	tokenattribute.line = line;
}

